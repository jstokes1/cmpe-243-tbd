#include "can_bus_initializer.h"

#include <stdio.h>

uint32_t can_bus_initializer__max_baudrate_kbps = 100;
uint16_t can_bus_initializer__max_rxq_size = 100;
uint16_t can_bus_initializer__max_txq_size = 100;

bool can_bus_initializer__initialize(can__num_e can_num, uint32_t baudrate_kbps, uint16_t rxq_size, uint16_t txq_size) {
  if (can_num >= can_max) {
    printf("Invalid can_num (%d).\n", can_num);
    return false;
  }
  if (baudrate_kbps > can_bus_initializer__max_baudrate_kbps) {
    printf("baudrate_kbps (%ld) exceeds max_baudrate_kbps (%ld).\n", baudrate_kbps,
           can_bus_initializer__max_baudrate_kbps);
    return false;
  }
  if ((rxq_size > can_bus_initializer__max_rxq_size) || (txq_size > can_bus_initializer__max_txq_size)) {
    printf("Max rxq_size (%d), max txq_size (%d), user input rxq_size (%d), txq_size (%d).\n",
           can_bus_initializer__max_rxq_size, can_bus_initializer__max_txq_size, rxq_size, txq_size);
    return false;
  }

  can__init(can_num, baudrate_kbps, rxq_size, txq_size, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can_num);
  return true;
}

void can_bus_initializer__bus_error_state_callback(uint32_t icr_value) {}

void can_bus_initializer__data_overrun_callback(uint32_t icr_value) {}