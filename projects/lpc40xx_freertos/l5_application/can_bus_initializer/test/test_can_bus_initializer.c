#include "unity.h"

// Include the Mocks
#include "Mockcan_bus.h"

// Include the source we wish to test
#include "can_bus_initializer.h"

void setUp(void) {}

void tearDown(void) {}

static uint32_t baudrate_kbps = 100;
static uint16_t rxq_size = 100;
static uint16_t txq_size = 100;

void test__can_bus_initializer__initialize(void) {
  can__init_ExpectAndReturn(can1, baudrate_kbps, rxq_size, txq_size, NULL, NULL, NULL);
  can__init_IgnoreArg_bus_off_cb();
  can__init_IgnoreArg_data_ovr_cb();

  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);

  TEST_ASSERT_TRUE(can_bus_initializer__initialize(can1, baudrate_kbps, rxq_size, txq_size));

  can__init_ExpectAndReturn(can2, baudrate_kbps, rxq_size, txq_size, NULL, NULL, NULL);
  can__init_IgnoreArg_bus_off_cb();
  can__init_IgnoreArg_data_ovr_cb();

  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can2);

  TEST_ASSERT_TRUE(can_bus_initializer__initialize(can2, baudrate_kbps, rxq_size, txq_size));
}

void test__can_bus_initializer__initialize_failed(void) {
  TEST_ASSERT_FALSE(can_bus_initializer__initialize(can_max, baudrate_kbps, rxq_size, txq_size));
  TEST_ASSERT_FALSE(
      can_bus_initializer__initialize(can1, can_bus_initializer__max_baudrate_kbps + 1, rxq_size, txq_size));
  TEST_ASSERT_FALSE(
      can_bus_initializer__initialize(can1, baudrate_kbps, can_bus_initializer__max_rxq_size + 1, txq_size));
  TEST_ASSERT_FALSE(
      can_bus_initializer__initialize(can1, baudrate_kbps, rxq_size, can_bus_initializer__max_txq_size + 1));
}