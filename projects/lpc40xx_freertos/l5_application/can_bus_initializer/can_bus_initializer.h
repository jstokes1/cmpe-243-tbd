#pragma once

#include <stdint.h>

#include "can_bus.h"

extern uint32_t can_bus_initializer__max_baudrate_kbps;
extern uint16_t can_bus_initializer__max_rxq_size;
extern uint16_t can_bus_initializer__max_txq_size;

bool can_bus_initializer__initialize(can__num_e can_num, uint32_t baudrate_kbps, uint16_t rxq_size, uint16_t txq_size);

void can_bus_initializer__bus_error_state_callback(uint32_t icr_value);

void can_bus_initializer__data_overrun_callback(uint32_t icr_value);