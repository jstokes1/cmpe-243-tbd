#pragma once

#include "project.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

void motor_controller__init(void);                                                 //
void motor_controller__process_messages(void);                                     //
void motor_controller__manage_mia_10hz(void);                                      //
void motor_controller__process_motor_cmd(void);                                    //
dbc_MOTOR_DEBUG_MSG_s motor_controller__get_motor_debug_msg(void);                 //
void motor_controller__set_motor_command(dbc_DRIVER_TO_MOTOR_CMD_s motor_command); //
void motor_controller__set_speed(uint8_t speed_from_driver, size_t current_rpm);   //
void motor_controller__set_direction(int16_t direction);                           //
void motor_controller__set_speed_pwm_cycle(float duty_cycle);
void motor_controller__handle_reversing(bool speed_up);
bool motor_controller__speed_up_or_down(uint16_t expected_rpm, size_t received_rpm);

/* Test functions */
void motor_controller__functional_motor_test(void);
