#include "motor_controller.h"

#include "can_bus.h"
#include "can_mia_configurations.h"
#include "delay.h"
#include "gpio.h"
#include "pwm1.h"

#include <stdio.h>

static dbc_DRIVER_TO_MOTOR_CMD_s motor_command = {0};
static dbc_MOTOR_DEBUG_MSG_s motor_debug_msg = {0};

static uint16_t rpm_table[] = {
    [0] = 0,    [1] = 300,  [2] = 250,  [3] = 200,  [4] = 175,  [5] = 0,    [6] = 175,  [7] = 200,
    [8] = 225,  [9] = 250,  [10] = 275, [11] = 300, [12] = 325, [13] = 350, [14] = 375, [15] = 400,
    [16] = 425, [17] = 450, [18] = 475, [19] = 500, [20] = 525, [21] = 550, [22] = 575, [23] = 600,
    [24] = 625, [25] = 650, [26] = 675, [27] = 700, [28] = 725, [29] = 750, [30] = 775, [31] = 800,
};

static float current_duty_cycle = 9.1f; // idle is 9.1f
static float min_forward_duty_cycle = 9.7f;

static float current_speed = 9.1F;
static const uint16_t rpm_buffer_above = 50;
static const uint16_t rpm_buffer_below = 50;
static const float pwm_duty_cycle_scale_up = 0.005f;
static const float pwm_duty_cycle_scale_down = 0.003f;

static const float idle_speed = 9.1F;
static const float idle_direction = 9.31F;
static size_t reset_routine_counter = 0U;

void motor_controller__set_speed(uint8_t speed_from_driver, size_t received_rpm) {
  static size_t reverse_routine_counter = 0U;
  const uint16_t expected_rpm = rpm_table[speed_from_driver];
  const bool within_range =
      ((received_rpm <= (expected_rpm + rpm_buffer_above)) && (received_rpm >= (expected_rpm - rpm_buffer_below)));
  if (within_range) {
    // do nothing, current duty cycle is a-ok
  } else {
    // adjustment to pwm cycle needed
    bool speed_up = motor_controller__speed_up_or_down(expected_rpm, received_rpm);
    if ((speed_from_driver == 5U) || (speed_from_driver == 0U)) {
      reverse_routine_counter = 0;
      current_duty_cycle = 9.1f;
    } else if (speed_from_driver > 5U) {
      reverse_routine_counter = 0;
      if (speed_up) {
        current_duty_cycle += pwm_duty_cycle_scale_up;
      } else {
        current_duty_cycle -= pwm_duty_cycle_scale_down;
      }
      if (current_duty_cycle < min_forward_duty_cycle) {
        current_duty_cycle = min_forward_duty_cycle;
      }
    } else {
      if (reverse_routine_counter > 30) {
        if (speed_up) {
          current_duty_cycle -= pwm_duty_cycle_scale_up;
        } else {
          current_duty_cycle += pwm_duty_cycle_scale_down;
        }
      } else if (reverse_routine_counter == 0U) {
        current_duty_cycle = 9.1f;
      } else if (reverse_routine_counter == 10U) {
        current_duty_cycle = 8.0f;
      } else if (reverse_routine_counter == 20U) {
        current_duty_cycle = 9.1f;
      } else if (reverse_routine_counter == 30U) {
        // reverse routine complete
        current_duty_cycle = 8.0f;
      }
      reverse_routine_counter++;
    }
  }
  motor_controller__set_speed_pwm_cycle(current_duty_cycle);
}

bool motor_controller__speed_up_or_down(uint16_t expected_rpm, size_t received_rpm) {
  return (received_rpm < expected_rpm);
}

void motor_controller__init(void) {
  gpio_s steer_gpio = gpio__construct_as_output(GPIO__PORT_2, 0);
  (void)gpio__set_function(steer_gpio, GPIO__FUNCTION_1);
  gpio_s speed_gpio = gpio__construct_as_output(GPIO__PORT_2, 2);
  gpio__reset(speed_gpio);
  delay__ms(500);
  (void)gpio__set_function(speed_gpio, GPIO__FUNCTION_1);
  pwm1__init_single_edge(60);
  pwm1__set_duty_cycle(PWM1__2_2, idle_speed);
  pwm1__set_duty_cycle(PWM1__2_0, idle_direction);
  delay__ms(3000);
}

void motor_controller__process_messages(void) {
  can__msg_t can_msg = {};

  // Receive all messages
  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };
    dbc_decode_DRIVER_TO_MOTOR_CMD(&motor_command, header, can_msg.data.bytes);
  }
}

void motor_controller__manage_mia_10hz(void) {
  // We are in 10hz slot, so increment MIA counter by 100ms
  const uint32_t mia_increment_value = 100;

  if (dbc_service_mia_DRIVER_TO_MOTOR_CMD(&motor_command, mia_increment_value)) {
    // TODO: indicate that DRIVER node is MIA
  }
}

void motor_controller__process_motor_cmd(void) {
  motor_controller__set_direction(motor_command.DRIVER_TO_MOTOR_steer);
  if (motor_command.DRIVER_TO_MOTOR_speed == 0U) {
    motor_command.DRIVER_TO_MOTOR_speed = 5U;
  }
  motor_controller__set_speed(motor_command.DRIVER_TO_MOTOR_speed, motor_command.DRIVER_TO_MOTOR_current_rpm);
  motor_debug_msg.MOTOR_DEBUG_MSG_echo_speed = motor_command.DRIVER_TO_MOTOR_speed;
  motor_debug_msg.MOTOR_DEBUG_MSG_echo_steer = motor_command.DRIVER_TO_MOTOR_steer;
  motor_debug_msg.MOTOR_DEBUG_MSG_echo_rpm = motor_command.DRIVER_TO_MOTOR_current_rpm;
}

dbc_MOTOR_DEBUG_MSG_s motor_controller__get_motor_debug_msg(void) { return motor_debug_msg; }

void motor_controller__set_motor_command(dbc_DRIVER_TO_MOTOR_CMD_s motor_cmd) { motor_command = motor_cmd; }

void motor_controller__set_speed_pwm_cycle(float duty_cycle) { pwm1__set_duty_cycle(PWM1__2_2, duty_cycle); }

void motor_controller__set_direction_pwm_cycle(float duty_cycle) { pwm1__set_duty_cycle(PWM1__2_0, duty_cycle); }

void motor_controller__set_direction(int16_t direction) {
  if (direction < -40) {
    motor_controller__set_direction_pwm_cycle(11.96); // 6.66
  } else if (direction > 40) {
    motor_controller__set_direction_pwm_cycle(6.66);
  } else {
    float duty_cycle = (9.31 - (direction / 40.0) * 2.65);
    motor_controller__set_direction_pwm_cycle(duty_cycle);
  }
}
