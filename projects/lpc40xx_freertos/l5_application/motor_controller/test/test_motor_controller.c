#include "unity.h"

#include "project.h"

#include "can_mia_configurations.h"

#include "Mockcan_bus.h"
#include "Mockdelay.h"
#include "Mockgpio.h"
#include "Mockpwm1.h"

#include "motor_controller.h"

void setUp(void) {}

void tearDown(void) {}

void test__motor_controller__process_motor_cmd(void) {
  // Check default motor_debug_msg
  dbc_MOTOR_DEBUG_MSG_s motor_debug_msg = motor_controller__get_motor_debug_msg();
  TEST_ASSERT_EQUAL(0, motor_debug_msg.MOTOR_DEBUG_MSG_echo_steer);
  TEST_ASSERT_EQUAL(0, motor_debug_msg.MOTOR_DEBUG_MSG_echo_speed);
  // Simulate MOTOR decoding DRIVER_TO_MOTOR_CMD message
  int test_steer = 2;
  int test_speed = 99;
  dbc_DRIVER_TO_MOTOR_CMD_s motor_command = {.DRIVER_TO_MOTOR_steer = test_steer, .DRIVER_TO_MOTOR_speed = test_speed};
  motor_controller__set_motor_command(motor_command);
  // Expects
  pwm1__set_duty_cycle_ExpectAnyArgs();
  pwm1__set_duty_cycle_ExpectAnyArgs();
  motor_controller__process_motor_cmd();
  // Verify MOTOR's internal motor_debug_msg is updated
  motor_debug_msg = motor_controller__get_motor_debug_msg();
  TEST_ASSERT_EQUAL(test_steer, motor_debug_msg.MOTOR_DEBUG_MSG_echo_steer);
  TEST_ASSERT_EQUAL(test_speed, motor_debug_msg.MOTOR_DEBUG_MSG_echo_speed);
}