#include "unity.h"

#include "Mockdelay.h"
#include "Mocki2c.h"

#include "i2c_lcd.h"

static const i2c_e i2c_lcd__sensor_bus = I2C__2;
static const uint8_t i2c_lcd__address = 0x4E;

void setUp(void) {}

void tearDown(void) {}

void test__i2c_lcd__send_command(void) {
  char command;
  uint8_t cmd_w[4];
  cmd_w[0] = (command & 0xf0) | 0x0C;
  cmd_w[1] = (command & 0xf0) | 0x08;
  cmd_w[2] = ((command << 4) & 0xf0) | 0x0C;
  cmd_w[3] = ((command << 4) & 0xf0) | 0x08;
  i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, cmd_w[0], cmd_w[1], true);
  i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, cmd_w[2], cmd_w[3], true);
  i2c_lcd__send_command(command);
}

void test__i2c_lcd__send_data(void) {
  char data;
  uint8_t data_w[4];
  data_w[0] = (data & 0xf0) | 0x0D;
  data_w[1] = (data & 0xf0) | 0x09;
  data_w[2] = ((data << 4) & 0xf0) | 0x0D;
  data_w[3] = ((data << 4) & 0xf0) | 0x09;
  i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, data_w[0], data_w[1], true);
  i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, data_w[2], data_w[3], true);
  i2c_lcd__send_data(data);
}

void test__i2c_lcd__clear(void) {
  char command = 0x00;
  uint8_t cmd_w[4];
  cmd_w[0] = (command & 0xf0) | 0x0C;
  cmd_w[1] = (command & 0xf0) | 0x08;
  cmd_w[2] = ((command << 4) & 0xf0) | 0x0C;
  cmd_w[3] = ((command << 4) & 0xf0) | 0x08;
  i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, cmd_w[0], cmd_w[1], true);
  i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, cmd_w[2], cmd_w[3], true);
  for (int i = 0; i < 100; i++) {
    char data = ' ';
    uint8_t data_w[4];
    data_w[0] = (data & 0xf0) | 0x0D;
    data_w[1] = (data & 0xf0) | 0x09;
    data_w[2] = ((data << 4) & 0xf0) | 0x0D;
    data_w[3] = ((data << 4) & 0xf0) | 0x09;
    i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, data_w[0], data_w[1], true);
    i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, data_w[2], data_w[3], true);
  }
  i2c_lcd__clear();
}

void test__i2c_lcd__starting_position(void) {
  uint8_t y;
  uint8_t x;
  char Starting_Addr;
  switch (y) {
  case 0:
    Starting_Addr = 0x00;
    break;
  case 1:
    Starting_Addr = 0x40;
    break;
  case 2:
    Starting_Addr = 0x14;
    break;
  case 3:
    Starting_Addr = 0x54;
    break;
  default:;
  }

  Starting_Addr += x;
  char command = 0x80 | Starting_Addr;
  uint8_t cmd_w[4];
  cmd_w[0] = (command & 0xf0) | 0x0C;
  cmd_w[1] = (command & 0xf0) | 0x08;
  cmd_w[2] = ((command << 4) & 0xf0) | 0x0C;
  cmd_w[3] = ((command << 4) & 0xf0) | 0x08;
  i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, cmd_w[0], cmd_w[1], true);
  i2c__write_single_ExpectAndReturn(i2c_lcd__sensor_bus, i2c_lcd__address, cmd_w[2], cmd_w[3], true);
  i2c_lcd__starting_position(y, x);
}

void test__starting_pos_y_0(void) {
  uint8_t y = 0;
  uint8_t x = 5;
  char Starting_Addr;
  switch (y) {
  case 0:
    Starting_Addr = 0x00;
    break;
  case 1:
    Starting_Addr = 0x40;
    break;
  case 2:
    Starting_Addr = 0x14;
    break;
  case 3:
    Starting_Addr = 0x54;
    break;
  default:;
  }
  TEST_ASSERT_EQUAL(Starting_Addr, 0x00);
  Starting_Addr += x;
  TEST_ASSERT_EQUAL(Starting_Addr, 5);
}

void test__starting_pos_y_1(void) {
  uint8_t y = 1;
  uint8_t x = 5;
  char Starting_Addr;
  switch (y) {
  case 0:
    Starting_Addr = 0x00;
    break;
  case 1:
    Starting_Addr = 0x40;
    break;
  case 2:
    Starting_Addr = 0x14;
    break;
  case 3:
    Starting_Addr = 0x54;
    break;
  default:;
  }
  TEST_ASSERT_EQUAL(Starting_Addr, 0x40);
  Starting_Addr += x;
  TEST_ASSERT_EQUAL(Starting_Addr, 69);
}

void test__starting_pos_y_2(void) {
  uint8_t y = 2;
  uint8_t x = 5;
  char Starting_Addr;
  switch (y) {
  case 0:
    Starting_Addr = 0x00;
    break;
  case 1:
    Starting_Addr = 0x40;
    break;
  case 2:
    Starting_Addr = 0x14;
    break;
  case 3:
    Starting_Addr = 0x54;
    break;
  default:;
  }
  TEST_ASSERT_EQUAL(Starting_Addr, 0x14);
  Starting_Addr += x;
  TEST_ASSERT_EQUAL(Starting_Addr, 25);
}