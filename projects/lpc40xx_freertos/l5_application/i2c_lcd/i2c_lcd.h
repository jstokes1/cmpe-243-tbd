#pragma once

#include <stdbool.h>
#include <stdint.h>

/// Enables acceleration sensor data update and returns true if we successfully detected the sensor
bool i2c_lcd__init(void);
void i2c_lcd__printf(char *str);
void i2c_lcd__send_command(char command);
void i2c_lcd__send_data(char data);
void i2c_lcd__clear(void);
void i2c_lcd__starting_position(uint8_t x, uint8_t y);