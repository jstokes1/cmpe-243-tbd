#include "project.h"

#include "can_mia_configurations.h"

// GEO MIA replacement values
const uint32_t dbc_mia_threshold_GPS_DESTINATION_LOCATION = 1000;
const dbc_GPS_DESTINATION_LOCATION_s dbc_mia_replacement_GPS_DESTINATION_LOCATION = {0};

// DRIVER MIA replacement values
const uint32_t dbc_mia_threshold_SENSOR_TO_DRIVER_SONARS = 1000;
const uint32_t dbc_mia_threshold_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG = 2000;
const uint32_t dbc_mia_threshold_GEO_STATUS = 1000;
const dbc_SENSOR_TO_DRIVER_SONARS_s dbc_mia_replacement_SENSOR_TO_DRIVER_SONARS = {0};
const dbc_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_s dbc_mia_replacement_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG = {0};
const dbc_GEO_STATUS_s dbc_mia_replacement_GEO_STATUS = {0};

// MOTOR MIA replacement values
const uint32_t dbc_mia_threshold_DRIVER_TO_MOTOR_CMD = 1000;
const dbc_DRIVER_TO_MOTOR_CMD_s dbc_mia_replacement_DRIVER_TO_MOTOR_CMD = {0};

// DEBUG MIA replacement values
const uint32_t dbc_mia_threshold_GEO_DEBUG_MSG = 2000;
const uint32_t dbc_mia_threshold_GEO_CURRENT_LOCATION_DEBUG = 2000;
const uint32_t dbc_mia_threshold_GEO_CURRENT_DESTINATION_LOCATION_DEBUG = 2000;
const uint32_t dbc_mia_threshold_MOTOR_DEBUG_MSG = 1000;
const uint32_t dbc_mia_threshold_SENSOR_TO_DRIVER_APP_CONNECTION_MSG = 1000;
const dbc_GEO_DEBUG_MSG_s dbc_mia_replacement_GEO_DEBUG_MSG = {0};
const dbc_GEO_CURRENT_LOCATION_DEBUG_s dbc_mia_replacement_GEO_CURRENT_LOCATION_DEBUG = {0};
const dbc_GEO_CURRENT_DESTINATION_LOCATION_DEBUG_s dbc_mia_replacement_GEO_CURRENT_DESTINATION_LOCATION_DEBUG = {
    .GEO_CURRENT_DESTINATION_LOCATION_DEBUG_latitude = 1, .GEO_CURRENT_DESTINATION_LOCATION_DEBUG_longitude = 1};
const dbc_MOTOR_DEBUG_MSG_s dbc_mia_replacement_MOTOR_DEBUG_MSG = {0};
const dbc_SENSOR_TO_DRIVER_APP_CONNECTION_MSG_s dbc_mia_replacement_SENSOR_TO_DRIVER_APP_CONNECTION_MSG = {0};
