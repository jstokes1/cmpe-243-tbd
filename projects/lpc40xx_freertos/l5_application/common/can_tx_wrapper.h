#pragma once

#include "can_bus.h"

#include "project.h"

void can_tx_wrapper__send_can_message(can__msg_t can_msg, const dbc_message_header_t header);