#include "board_leds.h"
#include "gpio.h"

void board_leds__turn_off_leds(void) {
  static const gpio_s led0 = {.port_number = 2, .pin_number = 3};
  static const gpio_s led1 = {.port_number = 1, .pin_number = 26};
  static const gpio_s led2 = {.port_number = 1, .pin_number = 24};
  static const gpio_s led3 = {.port_number = 1, .pin_number = 18};
  gpio__construct_as_output(led0.port_number, led0.pin_number);
  gpio__construct_as_output(led1.port_number, led1.pin_number);
  gpio__construct_as_output(led2.port_number, led2.pin_number);
  gpio__construct_as_output(led3.port_number, led3.pin_number);
  gpio__set(led0);
  gpio__set(led1);
  gpio__set(led2);
  gpio__set(led3);
}