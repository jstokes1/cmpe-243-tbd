#include "can_tx_wrapper.h"

void can_tx_wrapper__send_can_message(can__msg_t can_msg, const dbc_message_header_t header) {
  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  can__tx(can1, &can_msg, 0);
}
