#include "periodic_callbacks.h"

#include "project.h"

#include "board_leds.h"
#include "can_bus_initializer.h"
#include "can_tx_wrapper.h"

#include "motor_controller.h"

#include <stdio.h>

void send_motor_debug_message(void);

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  (void)can_bus_initializer__initialize(can1, 100, 100, 100);
  motor_controller__init();
  board_leds__turn_off_leds();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  motor_controller__manage_mia_10hz();
  send_motor_debug_message();
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  motor_controller__process_messages();
  motor_controller__process_motor_cmd();
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {}

void send_motor_debug_message(void) {
  can__msg_t tx_msg = {0};
  dbc_MOTOR_DEBUG_MSG_s motor_debug_msg = motor_controller__get_motor_debug_msg();
  const dbc_message_header_t msg_hdr = dbc_encode_MOTOR_DEBUG_MSG(tx_msg.data.bytes, &motor_debug_msg);
  can_tx_wrapper__send_can_message(tx_msg, msg_hdr);
}