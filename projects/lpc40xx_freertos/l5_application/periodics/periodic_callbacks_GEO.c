#include "periodic_callbacks.h"

#include "project.h"

#include "board_leds.h"
#include "can_bus_initializer.h"
#include "can_tx_wrapper.h"

#include "geo.h"
#include "lsm303_compass.h"
#include <stdio.h>

#include "gps.h"

void send_geo_status_to_driver(void);
void send_geo_dbg_msg_to_driver(void);
void send_geo_location_dbg_msg_to_driver(void);

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  (void)can_bus_initializer__initialize(can1, 100, 100, 100);
  (void)lsm303_compass__init();
  (void)gps__init();
  (void)gps__send_cmd();
  (void)board_leds__turn_off_leds();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  gps_coordinates_t coordinate = gps__get_coordinates();
  geo__update_current_coordinates(coordinate.latitude, coordinate.longitude);
  send_geo_location_dbg_msg_to_driver();
  send_geo_dbg_msg_to_driver();
  send_geo_current_destination_location_dbg_msg_to_sensor();
}

void periodic_callbacks__10Hz(uint32_t callback_count) { geo__manage_mia_10hz(); }

void periodic_callbacks__100Hz(uint32_t callback_count) {
  gps__run_once();
  geo__process_messages();
  geo__update_compass_heading();
  geo__update_current_destination();
  geo__update_destination_heading();
  geo__update_distance_to_destination();
  send_geo_status_to_driver();
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {}

void send_geo_status_to_driver(void) {
  can__msg_t tx_msg = {0};
  dbc_GEO_STATUS_s geo_status = geo__get_geo_status();
  const dbc_message_header_t header = dbc_encode_GEO_STATUS(tx_msg.data.bytes, &geo_status);
  can_tx_wrapper__send_can_message(tx_msg, header);
}

void send_geo_dbg_msg_to_driver(void) {
  can__msg_t tx_msg = {0};
  dbc_GEO_DEBUG_MSG_s gps_debug = gps__get_gps_debug();
  const dbc_message_header_t header = dbc_encode_GEO_DEBUG_MSG(tx_msg.data.bytes, &gps_debug);
  can_tx_wrapper__send_can_message(tx_msg, header);
}

void send_geo_location_dbg_msg_to_driver(void) {
  can__msg_t tx_msg = {0};
  dbc_GEO_CURRENT_LOCATION_DEBUG_s geo_current_location_debug = geo__get_geo_current_location_debug();
  const dbc_message_header_t header =
      dbc_encode_GEO_CURRENT_LOCATION_DEBUG(tx_msg.data.bytes, &geo_current_location_debug);
  can_tx_wrapper__send_can_message(tx_msg, header);
}

void send_geo_current_destination_location_dbg_msg_to_sensor(void) {
  can__msg_t tx_msg = {0};
  dbc_GEO_CURRENT_DESTINATION_LOCATION_DEBUG_s geo_current_destination_location_debug =
      geo__get_gps_current_destination_location_debug();
  const dbc_message_header_t header =
      dbc_encode_GEO_CURRENT_DESTINATION_LOCATION_DEBUG(tx_msg.data.bytes, &geo_current_destination_location_debug);
  can_tx_wrapper__send_can_message(tx_msg, header);
}