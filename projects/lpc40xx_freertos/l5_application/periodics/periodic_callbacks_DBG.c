#include "periodic_callbacks.h"

#include "project.h"

#include "can_bus_initializer.h"

#if NODE_NAME == GEO
#include "geo_calculation.h"
#elif NODE_NAME == SENSOR
#include "ultrasonic_sensor_app.h"
#elif NODE_NAME == DRIVER
#include "driver_logic.h"
#endif

#include <stdio.h>

#include "can_bus_initializer.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  (void)can_bus_initializer__initialize(can1, 100, 100, 100);
#if NODE_NAME == SENSOR
  ultrasonic_sensor_app__init();
#endif
#if NODE_NAME == DRIVER
  driver_logic__init();
#endif
#if NODE_NAME == GEO

#endif
}

void periodic_callbacks__1Hz(uint32_t callback_count) {}

void periodic_callbacks__10Hz(uint32_t callback_count) {}

void periodic_callbacks__100Hz(uint32_t callback_count) {}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {}
