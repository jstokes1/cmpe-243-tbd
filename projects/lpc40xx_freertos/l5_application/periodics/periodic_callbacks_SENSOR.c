#include "periodic_callbacks.h"

#include "project.h"

#include "ultrasonic_wrapper.h"

#include <stdio.h>

#include "board_leds.h"
#include "can_bus_initializer.h"
#include "can_tx_wrapper.h"

#include "esp32.h"
#include "esp32_task.h"
#include "uart3_init.h"

#include "semphr.h"

#include "sensor_app_debug_messages.h"

#include <string.h>

void send_sensor_to_driver_message(void);
void send_gps_destination_location_message(void);
void send_sensor_to_driver_wheel_encoder_message(void);

static dbc_SENSOR_TO_DRIVER_SONARS_s sensor_data = {0U};

static SemaphoreHandle_t sema;
static SemaphoreHandle_t phore;
static dbc_GPS_DESTINATION_LOCATION_s gps_destination_location = {0};
static char esp32__buffer[64] = {0};
static char buffer[256] = {0};

static float the_bs_conversion(char *stupid) {
  bool negative = false;
  size_t the_div_spot = 0;
  // size_t the_size = 0;
  int64_t the_big_num = 0;
  size_t the_spot = 0;
  while (true) {
    char the_char = stupid[the_spot];
    if (the_char == '\0') {
      break;
    } else if (the_char == '-') {
      negative = true;
    } else if (the_char == '.') {
      the_div_spot = the_spot;
    } else {
      the_big_num = (the_big_num * 10) + (the_char - '0');
    }
    the_spot++;
  }
  return (negative ? -1.0 : 1.0) * the_big_num / (pow(10, (the_spot - the_div_spot - 1)));
}

void esp32_udp_task(void *params) {
  // Wait for CLI task to output all data
  vTaskDelay(100);

  // Init and connect to wifi
  esp32_task__init();
  if (!esp32__wifi_connect("IsaaciPhone", "1m0g1er934pr")) {
    vTaskSuspend(NULL);
  }

  // Sample code that will connect to TCP/IP server once, and continuously send data
  esp32__upd_server_connect(1765);
  float lat = 0, lon = 0;
  while (1) {
    bool recv = esp32__hack_receive_response(esp32__buffer, sizeof(esp32__buffer));
    // sensor_app_debug_messages__get_geo_to_driver_status();
    dbc_GEO_CURRENT_LOCATION_DEBUG_s loc = sensor_app_debug_messages__get_geo_current_location();
    dbc_GEO_STATUS_s rot = sensor_app_debug_messages__get_geo_to_driver_status();
    dbc_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_s rpm = ultrasonic_wrapper__get_wheel_encoder_data();
    dbc_GEO_CURRENT_DESTINATION_LOCATION_DEBUG_s dest =
        sensor_app_debug_messages__get_geo_current_destination_location();
    snprintf(buffer, sizeof(buffer), "%f,%f,%i,%i,%f,%i,%i,%i,%i,%i,%f,%f\r\n", loc.GEO_CURRENT_LOCATION_DEBUG_latitude,
             loc.GEO_CURRENT_LOCATION_DEBUG_longitude, rot.GEO_STATUS_compass_heading,
             rot.GEO_STATUS_destination_heading, rot.GEO_STATUS_distance_to_destination,
             rpm.SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_rpm, sensor_data.SENSOR_TO_DRIVER_SONARS_front_left,
             sensor_data.SENSOR_TO_DRIVER_SONARS_front_middle, sensor_data.SENSOR_TO_DRIVER_SONARS_front_right,
             sensor_data.SENSOR_TO_DRIVER_SONARS_back, dest.GEO_CURRENT_DESTINATION_LOCATION_DEBUG_latitude,
             dest.GEO_CURRENT_DESTINATION_LOCATION_DEBUG_longitude);
    esp32__cipsend(buffer, strlen(buffer));
    if (recv) {
      xSemaphoreGive(phore);
      strtok(esp32__buffer, ":");
      lat = the_bs_conversion(strtok(NULL, ","));
      lon = the_bs_conversion(strtok(NULL, ","));
      // printf("%f, %f\n", lat, lon);
      xSemaphoreTake(sema, -1);
      gps_destination_location.GPS_DESTINATION_LOCATION_latitude = lat;
      gps_destination_location.GPS_DESTINATION_LOCATION_longitude = lon;
      xSemaphoreGive(sema);
    }
  }
}

/*****************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  (void)can_bus_initializer__initialize(can1, 100, 100, 100);
  ultrasonic_wrapper__initialize_all_ultrasonic_sensors();
  ultrasonic_wrapper__enable_gpio_interrupt_for_wheel_encoder();
  uart3_init();                                              // Also include:  uart3_init.h
  xTaskCreate(esp32_udp_task, "uart3", 4096, NULL, 1, NULL); // Include esp32_task.h
  sema = xSemaphoreCreateMutex();
  phore = xSemaphoreCreateBinary();
  board_leds__turn_off_leds();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // can__msg_t can_msg = {};
  // while (can__rx(can1, &can_msg, 0)) {
  // } // TODO: enable filtering
  sensor_app_debug_messages__process_all_messages();
  send_gps_destination_location_message();
  if (xSemaphoreTake(phore, 0)) {
    send_app_connection_message();
  }
  sensor_app_debug_messages__manage_mia_10hz();
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  if ((callback_count % 4) == 0) {
    sensor_data.SENSOR_TO_DRIVER_SONARS_front_left = ultrasonic_wrapper__get_front_left_value();
  } else if (callback_count % 4 == 1) {
    sensor_data.SENSOR_TO_DRIVER_SONARS_front_right = ultrasonic_wrapper__get_front_right_value();
  } else if (callback_count % 4 == 2) {
    sensor_data.SENSOR_TO_DRIVER_SONARS_front_middle = ultrasonic_wrapper__get_front_middle_value();
  } else if (callback_count % 4 == 3) {
    sensor_data.SENSOR_TO_DRIVER_SONARS_back = ultrasonic_wrapper__get_back_sensor_value();
  }
  send_sensor_to_driver_message();
  ultrasonic_wrapper__update_wheel_encoder_data();
  send_sensor_to_driver_wheel_encoder_message();
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {}

void send_sensor_to_driver_message(void) {
  can__msg_t can_msg = {0U};
  const dbc_message_header_t header = dbc_encode_SENSOR_TO_DRIVER_SONARS(can_msg.data.bytes, &sensor_data);
  can_tx_wrapper__send_can_message(can_msg, header);
}

void send_gps_destination_location_message(void) {
  can__msg_t can_msg = {0U};
  xSemaphoreTake(sema, -1);
  const dbc_message_header_t header =
      dbc_encode_GPS_DESTINATION_LOCATION(can_msg.data.bytes, &gps_destination_location);
  xSemaphoreGive(sema);
  can_tx_wrapper__send_can_message(can_msg, header);
}

void send_sensor_to_driver_wheel_encoder_message(void) {
  can__msg_t can_msg = {0U};
  dbc_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_s wheel_encoder_data = ultrasonic_wrapper__get_wheel_encoder_data();
  const dbc_message_header_t header =
      dbc_encode_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG(can_msg.data.bytes, &wheel_encoder_data);
  can_tx_wrapper__send_can_message(can_msg, header);
}

void send_app_connection_message(void) {
  can__msg_t can_msg = {0U};
  dbc_SENSOR_TO_DRIVER_APP_CONNECTION_MSG_s con;
  con.SENSOR_TO_DRIVER_APP_CONNECTION_MSG_app_connected = 1;
  const dbc_message_header_t header = dbc_encode_SENSOR_TO_DRIVER_APP_CONNECTION_MSG(can_msg.data.bytes, &con);
  can_tx_wrapper__send_can_message(can_msg, header);
}
