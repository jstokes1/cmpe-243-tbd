#include "periodic_callbacks.h"

#include "project.h"

#include "board_leds.h"
#include "can_bus_initializer.h"

#include "can_bus.h"
#include "can_tx_wrapper.h"

#include "driver_logic.h"

#include <stdio.h>

void send_driver_to_motor_command(void);

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  (void)can_bus_initializer__initialize(can1, 100, 100, 100);
  driver_logic__init();
  board_leds__turn_off_leds();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  driver_logic__manage_mia_10hz();
  driver_logic__manage_debug_menu_10hz(callback_count);
  driver_logic__update_leds(callback_count);
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  driver_logic__process_messages();
  driver_logic__process_geo_status(callback_count);
  send_driver_to_motor_command();
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {}

void send_driver_to_motor_command(void) {
  can__msg_t tx_msg = {0};
  dbc_DRIVER_TO_MOTOR_CMD_s motor_command = driver_logic__get_motor_command();
  const dbc_message_header_t header = dbc_encode_DRIVER_TO_MOTOR_CMD(tx_msg.data.bytes, &motor_command);
  can_tx_wrapper__send_can_message(tx_msg, header);
}