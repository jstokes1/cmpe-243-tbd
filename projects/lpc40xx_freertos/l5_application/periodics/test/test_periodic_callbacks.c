#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "project.h"

// Include the source we wish to test
#include "Mockcan_bus_initializer.h"
#include "Mockdriver_logic.h"
#include "Mocklsm303_compass.h"
#include "Mockultrasonic_wrapper.h"
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) { periodic_callbacks__initialize(); }
// void test__periodic_callbacks__initialize(void) { periodic_callbacks__initialize(); }

void test__periodic_callbacks__1Hz(void) { periodic_callbacks__1Hz(0); }

void test__periodic_callbacks__10Hz(void) { periodic_callbacks__10Hz(0); }

void test__periodic_callbacks__100Hz(void) { periodic_callbacks__100Hz(0); }

void test__periodic_callbacks__10000Hz(void) { periodic_callbacks__1000Hz(0); }