#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "project.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "Mockcan_bus_initializer.h"
#include "Mockdriver_logic.h"
#include "Mockultrasonic_sensor_app.h"
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_initializer__initialize_ExpectAndReturn(can1, 100, 100, 100, NULL);
#if NODE_NAME == SENSOR
  ultrasonic_sensor_app__init_Expect();
#endif
#if NODE_NAME == DRIVER
  driver_logic__init_Expect();
#endif
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) { periodic_callbacks__1Hz(0); }