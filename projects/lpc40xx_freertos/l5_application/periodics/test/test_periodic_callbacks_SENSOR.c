#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "project.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockcan_bus_initializer.h"
#include "Mockcan_tx_wrapper.h"
#include "Mockultrasonic_wrapper.h"

// Include the source we wish to test
#include "periodic_callbacks_SENSOR.c"

extern dbc_SENSOR_TO_DRIVER_SONARS_s sensor_data;
static dbc_GPS_DESTINATION_LOCATION_s gps_destination_location = {0};
static dbc_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_s wheel_encoder_data = {0};

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_initializer__initialize_ExpectAndReturn(can1, 100, 100, 100, NULL);
  ultrasonic_wrapper__initialize_all_ultrasonic_sensors_Expect();
  ultrasonic_wrapper__enable_gpio_interrupt_for_wheel_encoder_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  ultrasonic_wrapper__update_wheel_encoder_data_Expect();
  ultrasonic_wrapper__get_wheel_encoder_data_ExpectAndReturn(wheel_encoder_data);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  can_tx_wrapper__send_can_message_IgnoreArg_can_msg();
  can_tx_wrapper__send_can_message_IgnoreArg_header();
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz_callback_mod_zero_case(void) {
  ultrasonic_wrapper__get_front_left_value_ExpectAndReturn(0U);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  ultrasonic_wrapper__get_gps_destination_location_ExpectAndReturn(gps_destination_location);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__10Hz_callback_mod_one_case(void) {
  ultrasonic_wrapper__get_front_right_value_ExpectAndReturn(0U);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  ultrasonic_wrapper__get_gps_destination_location_ExpectAndReturn(gps_destination_location);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  periodic_callbacks__10Hz(1);
}

void test__periodic_callbacks__10Hz_callback_mod_two_case(void) {
  ultrasonic_wrapper__get_back_sensor_value_ExpectAndReturn(0U);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  ultrasonic_wrapper__get_gps_destination_location_ExpectAndReturn(gps_destination_location);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  periodic_callbacks__10Hz(2);
}

void test__periodic_callbacks__10Hz_callback_mod_three_case(void) {
  ultrasonic_wrapper__get_front_middle_value_ExpectAndReturn(0U);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  ultrasonic_wrapper__get_gps_destination_location_ExpectAndReturn(gps_destination_location);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  periodic_callbacks__10Hz(3);
}