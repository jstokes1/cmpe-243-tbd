#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "project.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "Mockcan_bus_initializer.h"
#include "Mockcan_tx_wrapper.h"
#include "Mockdriver_logic.h"
#include "periodic_callbacks_DRIVER.c"

static dbc_DRIVER_TO_MOTOR_CMD_s motor_command = {0};

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_initializer__initialize_ExpectAndReturn(can1, 100, 100, 100, NULL);
  driver_logic__init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) { periodic_callbacks__1Hz(0); }

void test__periodic_callbacks__10Hz(void) {
  driver_logic__manage_mia_10hz_Expect();
  driver_logic__manage_debug_menu_10hz_Expect();
  driver_logic__process_sensor_data_Expect();
  driver_logic__process_geo_status_Expect();
  driver_logic__get_motor_command_ExpectAndReturn(motor_command);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  can_tx_wrapper__send_can_message_IgnoreArg_can_msg();
  can_tx_wrapper__send_can_message_IgnoreArg_header();
  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__100Hz(void) {
  driver_logic__process_messages_Expect();
  periodic_callbacks__100Hz(0);
}

void test__periodic_callbacks__1000Hz(void) { periodic_callbacks__1000Hz(0); }