#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "project.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcan_bus.h"
#include "Mockcan_bus_initializer.h"
#include "Mockcan_tx_wrapper.h"
#include "Mockgpio.h"
#include "Mockmotor_controller.h"

// Include the source we wish to test
#include "periodic_callbacks_MOTOR.c"

static dbc_MOTOR_DEBUG_MSG_s motor_debug_message = {0};

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_initializer__initialize_ExpectAndReturn(can1, 100, 100, 100, NULL);
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__10Hz(void) {
  motor_controller__manage_mia_10hz_Expect();
  motor_controller__process_messages_Expect();
  motor_controller__get_motor_debug_msg_ExpectAndReturn(motor_debug_message);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  can_tx_wrapper__send_can_message_IgnoreArg_can_msg();
  can_tx_wrapper__send_can_message_IgnoreArg_header();
  periodic_callbacks__10Hz(0);
}