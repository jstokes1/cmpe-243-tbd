#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "project.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "Mockboard_leds.h"
#include "Mockcan_bus_initializer.h"
#include "Mockcan_tx_wrapper.h"
#include "Mockgeo.h"
#include "Mockgps.h"
#include "Mocklsm303_compass.h"
#include "periodic_callbacks_GEO.c"

static dbc_GEO_STATUS_s geo_status = {0};
static gps_coordinates_t current_coordinates = {0};
static dbc_GEO_CURRENT_LOCATION_DEBUG_s geo_current_location_debug = {0};
static dbc_GEO_DEBUG_MSG_s geo_debug_msg = {0};

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_bus_initializer__initialize_ExpectAndReturn(can1, 100, 100, 100, NULL);
  lsm303_compass__init_ExpectAndReturn(NULL);
  gps__init_Expect();
  gps__send_cmd_Expect();
  board_leds__turn_off_leds_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  gps__get_coordinates_ExpectAndReturn(current_coordinates);
  geo__update_current_coordinates_Expect(current_coordinates.latitude, current_coordinates.longitude);
  geo__get_geo_current_location_debug_ExpectAndReturn(geo_current_location_debug);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  can_tx_wrapper__send_can_message_IgnoreArg_can_msg();
  can_tx_wrapper__send_can_message_IgnoreArg_header();
  gps__get_gps_debug_ExpectAndReturn(geo_debug_msg);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  can_tx_wrapper__send_can_message_IgnoreArg_can_msg();
  can_tx_wrapper__send_can_message_IgnoreArg_header();
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz(void) {
  geo__manage_mia_10hz_Expect();
  geo__update_current_destination_Expect();
  geo__update_destination_heading_Expect();
  geo__update_distance_to_destination_Expect();
  geo__update_compass_heading_Expect();
  geo__get_geo_status_ExpectAndReturn(geo_status);
  can_tx_wrapper__send_can_message_ExpectAnyArgs();
  can_tx_wrapper__send_can_message_IgnoreArg_can_msg();
  can_tx_wrapper__send_can_message_IgnoreArg_header();
  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__100Hz(void) {
  gps__run_once_ExpectAndReturn(true);
  geo__process_messages_Expect();
  periodic_callbacks__100Hz(0);
}

void test__periodic_callbacks__1000Hz(void) { periodic_callbacks__1000Hz(0); }