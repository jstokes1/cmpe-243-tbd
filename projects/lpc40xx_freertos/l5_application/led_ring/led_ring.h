#pragma once
#include <stdint.h>

void configure_ssp2_pins(void);
void init_led_ring(uint32_t spi_clock_mhz, float distance_stop_threshold_value);
void set_led_ring(uint8_t brightness, uint8_t blue, uint8_t green, uint8_t red);
void led_ring_single_led_off(void);
void led_ring_starting_frame(void);
void led_ring_ending_frame(void);
void led_ring_all_on(uint8_t brightness, uint8_t blue, uint8_t green, uint8_t red);
void led_ring_handler_compass(uint16_t compass_heading, uint16_t angular_difference, uint32_t distance,
                              uint8_t brightness, uint8_t blue, uint8_t green, uint8_t red);