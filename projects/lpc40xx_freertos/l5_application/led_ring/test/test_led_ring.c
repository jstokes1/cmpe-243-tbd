#include "unity.h"

#include "Mockdelay.h"
#include "Mockgpio.h"
#include "Mockssp2.h"

#include "led_ring.h"

void setUp(void) {}

void tearDown(void) {}

void test__configure_ssp2_pins(void) {
  gpio_s gpio = {0};
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 0, GPIO__FUNCTION_4, gpio);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 1, GPIO__FUNCTION_4, gpio);
  configure_ssp2_pins();
}

void test__init_led_ring(void) {
  uint32_t spi_clock_mhz;
  float distance_stop_threshold_value;
  gpio_s gpio = {0};
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 0, GPIO__FUNCTION_4, gpio);
  gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_1, 1, GPIO__FUNCTION_4, gpio);
  ssp2__initialize_Expect(spi_clock_mhz);
  distance_stop_threshold_value = distance_stop_threshold_value;
  init_led_ring(spi_clock_mhz, distance_stop_threshold_value);
}

void test__set_led_ring(void) {
  uint8_t brightness, blue, green, red;
  uint8_t byte_to_transmit = {0};
  ssp2__exchange_byte_ExpectAndReturn(brightness, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(blue, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(green, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(red, byte_to_transmit);
  delay__us_Expect(10);
  set_led_ring(brightness, blue, green, red);
}

void test__led_ring_single_led_off(void) {
  uint8_t byte_to_transmit = {0};
  ssp2__exchange_byte_ExpectAndReturn(0xE0, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0x00, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0x00, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0x00, byte_to_transmit);
  delay__us_Expect(10);
  led_ring_single_led_off();
}

void test__led_ring_starting_frame(void) {
  uint8_t byte_to_transmit = {0};
  ssp2__exchange_byte_ExpectAndReturn(0x00, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0x00, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0x00, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0x00, byte_to_transmit);
  led_ring_starting_frame();
}

void test__led_ring_ending_frame(void) {
  uint8_t byte_to_transmit = {0};
  ssp2__exchange_byte_ExpectAndReturn(0xFF, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0xFF, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0xFF, byte_to_transmit);
  ssp2__exchange_byte_ExpectAndReturn(0xFF, byte_to_transmit);
  led_ring_ending_frame();
}
