#include "led_ring.h"
#include "delay.h"
#include "gpio.h"
#include "ssp2.h"
#include <stdint.h>
#include <stdio.h>

static float distance_stop_threshold = 0;

void configure_ssp2_pins(void) {
  gpio__construct_with_function(GPIO__PORT_1, 0,
                                GPIO__FUNCTION_4); // SSP2_SCK -> CI
  gpio__construct_with_function(GPIO__PORT_1, 1,
                                GPIO__FUNCTION_4); // SSP2_MOSI -> DI
}
void init_led_ring(uint32_t spi_clock_mhz, float distance_stop_threshold_value) {
  configure_ssp2_pins();
  ssp2__initialize(spi_clock_mhz);
  distance_stop_threshold = distance_stop_threshold_value;
}
void set_led_ring(uint8_t brightness, uint8_t blue, uint8_t green, uint8_t red) {
  ssp2__exchange_byte(brightness);
  ssp2__exchange_byte(blue);
  ssp2__exchange_byte(green);
  ssp2__exchange_byte(red);
  delay__us(10);
}
// turns off led_ring
void led_ring_single_led_off(void) {
  ssp2__exchange_byte(0xE0);
  ssp2__exchange_byte(0x00);
  ssp2__exchange_byte(0x00);
  ssp2__exchange_byte(0x00);
  delay__us(10);
}
void led_ring_all_on(uint8_t brightness, uint8_t blue, uint8_t green, uint8_t red) {

  for (int f = 0; f < 20; f++) {
    ssp2__exchange_byte(brightness);
    ssp2__exchange_byte(blue);
    ssp2__exchange_byte(green);
    ssp2__exchange_byte(red);
    delay__us(10);
  }
}
// starting frame all 32 bits 0
void led_ring_starting_frame(void) {
  ssp2__exchange_byte(0x00);
  ssp2__exchange_byte(0x00);
  ssp2__exchange_byte(0x00);
  ssp2__exchange_byte(0x00);
}
// ending frame first 10 bits 0 the rest 1// could be change to 0xFFFFFFFF later on
void led_ring_ending_frame(void) {
  ssp2__exchange_byte(0xFF);
  ssp2__exchange_byte(0xFF);
  ssp2__exchange_byte(0xFF);
  ssp2__exchange_byte(0xFF);
}

static uint16_t map_heading_to_led(uint16_t heading) {
  if (heading == 0) {
    return 0;
  } else {
    return 20 - heading;
  }
}

void led_ring_handler_compass(uint16_t compass_heading, uint16_t angular_difference, uint32_t distance,
                              uint8_t brightness, uint8_t blue, uint8_t green, uint8_t red) {
  led_ring_starting_frame();
  uint16_t compass_led =
      map_heading_to_led((360 - compass_heading) / 360.00 * 20.00); // true north, to change get rid of (360

  uint16_t destination_led; // -)
  if (angular_difference == 0xFFFF) {
    destination_led = 0xFFFF;
  } else {
    destination_led = map_heading_to_led(angular_difference / 360.00 * 20.00);
  }

  // destination_led = map_heading_to_led(value2 / 360.00 * 20.00);
  uint16_t led_ring[20] = {0}; // all led_ring set to 0
  led_ring[(compass_led)] = 1; // turn on compass led

  if (destination_led != 0xFFFF) {
    if (destination_led >= 10) {
      destination_led = destination_led - 10;
    } else if (destination_led < 10) {
      destination_led = destination_led + 10;
    }
    led_ring[(destination_led)] = 1; // turn on destination angular difference led
  }

  led_ring[0] = 1; /// turn on refference led

  for (int f = 0; f < 20; f++) {
    if (led_ring[f]) {
      if (0 == destination_led && f == 0) {
        set_led_ring(0xE3, 0x00, 0xFF, 0x00); // if destination
      } else if (f == 0) {
        set_led_ring(0xE3, 0xFF, 0x00, 0x00); // north is blue
      } else if (f == destination_led && destination_led != 0xFFFF) {
        set_led_ring(0xE3, 0x00, 0xFF, 0xFF); // destination heading is green
      } else if (f == compass_led) {
        set_led_ring(0xE3, 0x00, 0x00, 0xFF); // compass heading is red
      }
    } else if (!led_ring[f] && (distance > distance_stop_threshold || destination_led == 0xFFFF)) {
      led_ring_single_led_off(); // turns of the rest of led_rings
    } else if (!led_ring[f] && (distance <= distance_stop_threshold) && (destination_led != 0xFFFF)) {
      int color_value1 = rand() % 255;
      int color_value2 = rand() % 255;
      int color_value3 = rand() % 255;
      set_led_ring(0xE1, color_value1, color_value2, color_value3); // destination reached LED shows party colors
    }
  }
  led_ring_ending_frame();
  // this will only work to display the compass for a 20 led_rings ring
}