#include <stdbool.h>
#include <stdint.h>

#include "unity.h"

#include "Mockcan_bus.h"
#include "Mockgpio.h"
#include "Mockgpio_isr.h"
#include "Mocki2c.h"
#include "Mocklpc_peripherals.h"
#include "Mockultrasonic.h"
#include "project.h"

// Include the source we wish to test
#include "ultrasonic_wrapper.h"

static const uint8_t front_middle_sensor_slave_address = 0x22;
static const uint8_t front_left_sensor_slave_address = 0x26;
static const uint8_t front_right_sensor_slave_address = 0x2A;
static const uint8_t back_sensor_slave_address = 0xA0;

void setUp(void) {}

void tearDown(void) {}

void test__ultrasonic_wrapper__initialize_all_ultrasonic_sensors(void) {
  ultrasonic__initialize_Expect();
  ultrasonic__passive_measurement_mode_Expect(front_left_sensor_slave_address);
  ultrasonic__passive_measurement_mode_Expect(front_middle_sensor_slave_address);
  ultrasonic__passive_measurement_mode_Expect(front_right_sensor_slave_address);
  ultrasonic__passive_measurement_mode_Expect(back_sensor_slave_address);
  ultrasonic_wrapper__initialize_all_ultrasonic_sensors();
}

void test__slave_addresses(void) {
  TEST_ASSERT_EQUAL(0x22, front_middle_sensor_slave_address);
  TEST_ASSERT_EQUAL(0x26, front_left_sensor_slave_address);
  TEST_ASSERT_EQUAL(0x2A, front_right_sensor_slave_address);
  TEST_ASSERT_EQUAL(0xA0, back_sensor_slave_address);
}

void test__calculate_rpm_zero(void) {
  uint32_t rising_edges = 0;
  static const float rising_edges_per_revolution = 20.0f;
  static const float seconds_per_min = 60.0f;
  float value = (float)(((rising_edges * 10) / rising_edges_per_revolution) * seconds_per_min);
  TEST_ASSERT_EQUAL(0, value);
}
void test__calculate_rpm_low(void) {
  uint32_t rising_edges = 5;
  static const float rising_edges_per_revolution = 20.0f;
  static const float seconds_per_min = 60.0f;
  float value = (float)(((rising_edges * 10) / rising_edges_per_revolution) * seconds_per_min);
  TEST_ASSERT_EQUAL(150, value);
}

void test__calculate_rpm_high(void) {
  uint32_t rising_edges = 100;
  static const float rising_edges_per_revolution = 20.0f;
  static const float seconds_per_min = 60.0f;
  float value = (float)(((rising_edges * 10) / rising_edges_per_revolution) * seconds_per_min);
  TEST_ASSERT_EQUAL(3000, value);
}