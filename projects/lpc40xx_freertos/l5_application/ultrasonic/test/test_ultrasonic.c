#include <stdbool.h>
#include <stdint.h>

#include "unity.h"

#include "Mockclock.h"
#include "Mocki2c.h"
#include "Mocksemphr.h"

// Include the source we wish to test
#include "ultrasonic.c"

static const uint8_t sensor_slave_address = 0x22;

void setUp(void) {}

void tearDown(void) {}

void test__ultrasonic__initialize(void) {
  static const uint32_t expected_i2c_speed = 100000U;
  static const uint32_t expected_sys_clock = 96000000U;

  clock__get_peripheral_clock_hz_ExpectAndReturn(expected_sys_clock);
  i2c__initialize_Expect(I2C__2, expected_i2c_speed, expected_sys_clock, &binary_semaphore_struct, &mutex_struct);
  ultrasonic__initialize();
}

void test__ultrasonic__enable_automatic_measurement_mode(void) {
  const uint8_t automatic_measurement_mode = 0xA0;
  const uint8_t control_register_address = 0x07;

  i2c__write_single_ExpectAndReturn(I2C__2, sensor_slave_address, control_register_address, automatic_measurement_mode,
                                    true);
  ultrasonic__enable_automatic_measurement_mode(sensor_slave_address);
}

void test__ultrasonic__get_distance(void) {
  uint8_t low_bits = 0, high_bits = 0;
  const float cm_to_inches = 2.54;
  const uint8_t distance_high_bits_register = 0x03;
  const uint8_t distance_low_bits_register = 0x04;
  i2c__read_single_ExpectAndReturn(I2C__2, sensor_slave_address, distance_high_bits_register, true);
  i2c__read_single_ExpectAndReturn(I2C__2, sensor_slave_address, distance_low_bits_register, true);
  low_bits = ultrasonic__get_distance(sensor_slave_address) / cm_to_inches;
}