#include "ultrasonic.h"

static StaticSemaphore_t binary_semaphore_struct;
static StaticSemaphore_t mutex_struct;

static const uint8_t distance_high_bits_register = 0x03;
static const uint8_t distance_low_bits_register = 0x04;

static void read_distance_values_from_sensor_registers(uint8_t *low_bits, uint8_t *high_bits,
                                                       uint8_t sensor_slave_address) {
  *high_bits = i2c__read_single(I2C__2, sensor_slave_address, distance_high_bits_register);
  *low_bits = i2c__read_single(I2C__2, sensor_slave_address, distance_low_bits_register);
}

void ultrasonic__initialize(void) {
  const uint32_t i2c_speed_hz = 100000;
  i2c__initialize(I2C__2, i2c_speed_hz, clock__get_peripheral_clock_hz(), &binary_semaphore_struct, &mutex_struct);
}

void ultrasonic__enable_automatic_measurement_mode(uint8_t sensor_slave_address) {
  const uint8_t automatic_measurement_mode = 0xA0;
  const uint8_t control_register_address = 0x07;
  i2c__write_single(I2C__2, sensor_slave_address, control_register_address, automatic_measurement_mode);
}

uint8_t ultrasonic__get_distance(uint8_t sensor_slave_address) {
  uint8_t low_bits = 0, high_bits = 0;
  const float cm_to_inches = 2.54;
  read_distance_values_from_sensor_registers(&low_bits, &high_bits, sensor_slave_address);
  return low_bits / cm_to_inches;
}

void ultrasonic__passive_measurement_mode(uint8_t slave_address) {
  const uint8_t passive_measurement_mode = 0x00;
  const uint8_t control_register = 0x07;
  i2c__write_single(I2C__2, slave_address, control_register, passive_measurement_mode);
}

uint8_t ultrasonic__single_sensor_read(uint8_t slave_address) {
  return i2c__read_single(I2C__2, slave_address, distance_low_bits_register);
}