#include "sensor_app_debug_messages.h"

#include "can_bus.h"
#include "project.h"

static bool driver_mia = false;
static bool geo_mia = false;
static bool motor_mia = false;

static dbc_DRIVER_TO_MOTOR_CMD_s driver_to_motor_commands = {0};
static dbc_DRIVER_DEBUG_MSG_s driver_debug_messages = {0};
static dbc_GEO_STATUS_s geo_to_driver_status = {0};
static dbc_GEO_DEBUG_MSG_s geo_debug_messages = {0};
static dbc_GEO_CURRENT_LOCATION_DEBUG_s geo_current_location = {0};
static dbc_GEO_CURRENT_DESTINATION_LOCATION_DEBUG_s geo_current_destination_location = {0};
static dbc_MOTOR_DEBUG_MSG_s motor_debug_messages = {0};

void sensor_app_debug_messages__process_all_messages(void) {
  // Receive all messages
  can__msg_t can_msg = {};
  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };
    // decode here
    if (dbc_decode_DRIVER_TO_MOTOR_CMD(&driver_to_motor_commands, header, can_msg.data.bytes)) {
      driver_mia = false;
    }
    if (dbc_decode_GEO_STATUS(&geo_to_driver_status, header, can_msg.data.bytes)) {
      geo_mia = false;
    }
    if (dbc_decode_GEO_DEBUG_MSG(&geo_debug_messages, header, can_msg.data.bytes)) {
      geo_mia = false;
    }
    if (dbc_decode_GEO_CURRENT_LOCATION_DEBUG(&geo_current_location, header, can_msg.data.bytes)) {
      geo_mia = false;
    }
    if (dbc_decode_GEO_CURRENT_DESTINATION_LOCATION_DEBUG(&geo_current_destination_location, header,
                                                          can_msg.data.bytes)) {
      geo_mia = false;
    }
    if (dbc_decode_MOTOR_DEBUG_MSG(&motor_debug_messages, header, can_msg.data.bytes)) {
      motor_mia = false;
    }
  }
}

dbc_DRIVER_TO_MOTOR_CMD_s sensor_app_debug_messages__get_driver_to_motor_commands(void) {
  return driver_to_motor_commands;
}

dbc_GEO_STATUS_s sensor_app_debug_messages__get_geo_to_driver_status(void) { return geo_to_driver_status; }

dbc_GEO_DEBUG_MSG_s sensor_app_debug_messages__get_geo_debug_messages(void) { return geo_debug_messages; }

dbc_GEO_CURRENT_LOCATION_DEBUG_s sensor_app_debug_messages__get_geo_current_location(void) {
  return geo_current_location;
}

dbc_GEO_CURRENT_DESTINATION_LOCATION_DEBUG_s sensor_app_debug_messages__get_geo_current_destination_location(void) {
  return geo_current_destination_location;
}

dbc_MOTOR_DEBUG_MSG_s sensor_app_debug_messages__get_motor_debug_messages(void) { return motor_debug_messages; }

void sensor_app_debug_messages__manage_mia_10hz(void) {
  // We are in 10hz slot, so increment MIA counter by 100ms
  const uint32_t mia_increment_value = 100;

  if (dbc_service_mia_DRIVER_TO_MOTOR_CMD(&driver_to_motor_commands, mia_increment_value)) {
    driver_mia = true;
  }
  if (dbc_service_mia_GEO_STATUS(&geo_to_driver_status, mia_increment_value)) {
    geo_mia = true;
  }
  if (dbc_service_mia_GEO_DEBUG_MSG(&geo_debug_messages, mia_increment_value)) {
    geo_mia = true;
  }
  if (dbc_service_mia_GEO_CURRENT_LOCATION_DEBUG(&geo_current_location, mia_increment_value)) {
    geo_mia = true;
  }
  if (dbc_service_mia_GEO_CURRENT_DESTINATION_LOCATION_DEBUG(&geo_current_destination_location, mia_increment_value)) {
    geo_mia = true;
  }
  if (dbc_service_mia_MOTOR_DEBUG_MSG(&motor_debug_messages, mia_increment_value)) {
    motor_mia = true;
  }
}

bool sensor_app_debug_messages__is_driver_mia(void) { return driver_mia; }

bool sensor_app_debug_messages__is_geo_mia(void) { return geo_mia; }

bool sensor_app_debug_messages__is_motor_mia(void) { return motor_mia; }
