
#include "gpio_isr.h"
#include "lpc40xx.h"
#include "stdio.h"

static function_pointer_t gpio0_rising_callbacks[32];
static function_pointer_t gpio0_falling_callbacks[32];

static int check_pin_that_generated_interrupt(void);
static void clear_pin_interrupt(int pin);

void gpio_isr__gpio0_attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback) {
  // 1) Store the callback based on the pin at gpio0_callbacks
  // 2) Configure GPIO 0 pin for rising or falling edge
  if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
    gpio0_falling_callbacks[pin] = callback;
    LPC_GPIOINT->IO0IntEnF |= (1 << pin);
  } else if (interrupt_type == GPIO_INTR__RISING_EDGE) {
    gpio0_rising_callbacks[pin] = callback;
    LPC_GPIOINT->IO0IntEnR |= (1 << pin);
  } else {
    fprintf(stderr, "GPIO Interrupt type not handled\n");
  }
}

void gpio_isr__interrupt_dispatcher(void) {
  const int pin_that_generated_interrupt = check_pin_that_generated_interrupt();
  function_pointer_t attached_user_handler;
  if (LPC_GPIOINT->IO0IntEnR & (1 << pin_that_generated_interrupt)) {
    attached_user_handler = gpio0_rising_callbacks[pin_that_generated_interrupt];
  } else if (LPC_GPIOINT->IO0IntEnF & (1 << pin_that_generated_interrupt)) {
    attached_user_handler = gpio0_falling_callbacks[pin_that_generated_interrupt];
  } else if (pin_that_generated_interrupt == -1) {
    fprintf(stderr, "Could not find pin that generated the interrupt\n");
    return;
  }

  // Invoke the user registered callback, and then clear the interrupt
  attached_user_handler();
  clear_pin_interrupt(pin_that_generated_interrupt);
}

static int check_pin_that_generated_interrupt(void) {
  if (LPC_GPIOINT->IntStatus & (1 << 0)) {
    for (int i = 0; i < 32; i++) {
      if ((LPC_GPIOINT->IO0IntEnR & (1 << i)) && (LPC_GPIOINT->IO0IntStatR & (1 << i))) {
        return i;
      } else if ((LPC_GPIOINT->IO0IntEnF & (1 << i)) && (LPC_GPIOINT->IO0IntStatF & (1 << i))) {
        return i;
      }
    }
  } else if (LPC_GPIOINT->IntStatus & (1 << 2)) {
    for (int i = 0; i < 32; i++) {
      if ((LPC_GPIOINT->IO2IntEnR & (1 << i)) && (LPC_GPIOINT->IO2IntStatR & (1 << i))) {
        return i;
      } else if ((LPC_GPIOINT->IO2IntEnF & (1 << i)) && (LPC_GPIOINT->IO2IntStatF & (1 << i))) {
        return i;
      }
    }
  }
  return -1;
}

static void clear_pin_interrupt(int pin) { LPC_GPIOINT->IO0IntClr |= (1 << pin); }