/* Reference: https://wiki.dfrobot.com/URM09_Ultrasonic_Sensor_(Gravity-I2C)_(V1.0)_SKU_SEN0304 */

#pragma once

#include <stdint.h>
#include <stdio.h>

#include "clock.h"
#include "i2c.h"
#include "semphr.h"

void ultrasonic__initialize(void);

void ultrasonic__enable_automatic_measurement_mode(uint8_t sensor_slave_address);

uint8_t ultrasonic__get_distance(uint8_t sensor_slave_address);

void ultrasonic__passive_measurement_mode(uint8_t slave_address);

uint8_t ultrasonic__single_sensor_read(uint8_t slave_address);