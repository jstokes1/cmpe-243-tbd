#include "ultrasonic_wrapper.h"
#include "can_bus.h"
#include "i2c.h"
#include "project.h"
#include "ultrasonic.h"

#include "gpio.h"
#include "gpio_isr.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

#define RPM_SAMPLES 10

static const uint8_t front_middle_sensor_slave_address = 0x22;
static const uint8_t front_left_sensor_slave_address = 0x26;
static const uint8_t front_right_sensor_slave_address = 0x2A;
static const uint8_t back_sensor_slave_address = 0xA0;

static const uint8_t sen0304_command_register = 0x08;
static const uint8_t sen0304_distance_command = 0x01;

static dbc_SENSOR_TO_DRIVER_SONARS_s sensor_data = {0};
static dbc_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_s wheel_encoder_data = {0};

static volatile uint32_t wheel_encoder_rising_edge_counter = 0;
static const float rising_edges_per_revolution = 20.0f;
static const float seconds_per_min = 60.0f;
static const gpio_s wheel_encoder_gpio = {.port_number = 0, .pin_number = 22};

static void wheel_encoder_rising_edge_isr(void) { wheel_encoder_rising_edge_counter++; }

static void reset_wheel_encoder_counter(void) { wheel_encoder_rising_edge_counter = 0; }

float calculate_rpm(uint32_t rising_edges) {
  static uint32_t readings[RPM_SAMPLES] = {0};
  static uint8_t index = 0;
  uint32_t rising_edges_sum = 0;
  // if (rising_edges > 5) {
  //   rising_edges = 1;
  // }
  readings[index] = rising_edges;
  index = (index + 1) % RPM_SAMPLES;

  for (int i = 0; i < RPM_SAMPLES; i++) {
    rising_edges_sum += readings[i];
  }
  // printf("%i %f\n", rising_edges, value);
  return (rising_edges_sum * (100.0 / RPM_SAMPLES) * seconds_per_min) / rising_edges_per_revolution;
}

void ultrasonic_wrapper__initialize_all_ultrasonic_sensors(void) {
  ultrasonic__initialize();
  ultrasonic__passive_measurement_mode(front_left_sensor_slave_address);
  ultrasonic__passive_measurement_mode(front_middle_sensor_slave_address);
  ultrasonic__passive_measurement_mode(front_right_sensor_slave_address);
  ultrasonic__passive_measurement_mode(back_sensor_slave_address);
}

uint8_t ultrasonic_wrapper__get_front_left_value(void) {
  (void)i2c__write_single(I2C__2, front_left_sensor_slave_address, sen0304_command_register, sen0304_distance_command);
  return ultrasonic__single_sensor_read(front_left_sensor_slave_address);
}

uint8_t ultrasonic_wrapper__get_front_right_value(void) {
  (void)i2c__write_single(I2C__2, front_right_sensor_slave_address, sen0304_command_register, sen0304_distance_command);
  return ultrasonic__single_sensor_read(front_right_sensor_slave_address);
}

uint8_t ultrasonic_wrapper__get_front_middle_value(void) {
  (void)i2c__write_single(I2C__2, front_middle_sensor_slave_address, sen0304_command_register,
                          sen0304_distance_command);
  return ultrasonic__single_sensor_read(front_middle_sensor_slave_address);
}

uint8_t ultrasonic_wrapper__get_back_sensor_value(void) {
  (void)i2c__write_single(I2C__2, back_sensor_slave_address, sen0304_command_register, sen0304_distance_command);
  return ultrasonic__single_sensor_read(back_sensor_slave_address);
}

void ultrasonic_wrapper__enable_gpio_interrupt_for_wheel_encoder(void) {
  gpio__construct_as_input(GPIO__PORT_0, wheel_encoder_gpio.pin_number);
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio_isr__interrupt_dispatcher, NULL);
  gpio_isr__gpio0_attach_interrupt(wheel_encoder_gpio.pin_number, GPIO_INTR__RISING_EDGE,
                                   wheel_encoder_rising_edge_isr);
  NVIC_EnableIRQ(GPIO_IRQn);
}

void ultrasonic_wrapper__update_wheel_encoder_data(void) {
  wheel_encoder_data.SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_rpm = calculate_rpm(wheel_encoder_rising_edge_counter);
  // printf("rpm! %d\n", wheel_encoder_data.SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_rpm);
  reset_wheel_encoder_counter();
}

dbc_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_s ultrasonic_wrapper__get_wheel_encoder_data(void) { return wheel_encoder_data; }
