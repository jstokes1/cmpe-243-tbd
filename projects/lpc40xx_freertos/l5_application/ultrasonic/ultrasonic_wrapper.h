#pragma once

#include "project.h"

#include "can_bus.h"

void ultrasonic_wrapper__initialize_all_ultrasonic_sensors(void);

void ultrasonic_wrapper__enable_gpio_interrupt_for_wheel_encoder(void);

void ultrasonic_wrapper__update_wheel_encoder_data(void);

float calculate_rpm(uint32_t rising_edges);

dbc_GPS_DESTINATION_LOCATION_s ultrasonic_wrapper__get_gps_destination_location(void);

dbc_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_s ultrasonic_wrapper__get_wheel_encoder_data(void);

dbc_GPS_DESTINATION_LOCATION_s ultrasonic_wrapper__get_gps_destination_location(void);

uint8_t ultrasonic_wrapper__get_front_left_value(void);

uint8_t ultrasonic_wrapper__get_front_right_value(void);

uint8_t ultrasonic_wrapper__get_front_middle_value(void);

uint8_t ultrasonic_wrapper__get_back_sensor_value(void);
