#include "unity.h"

#include "waypoint.h"

#include "coordinate_calculation.h"

static const gps_coordinates_t waypoints[] = {
    {37.338966, -121.880890}, {37.339096, -121.880820}, {37.339228, -121.880781}, {37.339358, -121.880887},
    {37.339415, -121.881075}, {37.339434, -121.881229}, {37.339235, -121.880628}, {37.339345, -121.880416},
    {37.339412, -121.880750}, {37.339520, -121.880498}};

static gps_coordinates_t close_to_node_0 = {37.338834, -121.880758};
static gps_coordinates_t close_to_node_1 = {37.339062, -121.880916};
static gps_coordinates_t close_to_node_2 = {37.339211, -121.880783};
static gps_coordinates_t close_to_node_3 = {37.339421, -121.880864};
static gps_coordinates_t close_to_node_4 = {37.339444, -121.881014};
static gps_coordinates_t close_to_node_5 = {37.339546, -121.881261};
static gps_coordinates_t close_to_node_6 = {37.339280, -121.880663};
static gps_coordinates_t close_to_node_7 = {37.339330, -121.880402};
static gps_coordinates_t close_to_node_8 = {37.339428, -121.880754};
static gps_coordinates_t close_to_node_9 = {37.339574, -121.880560};

void test_waypoint__closest_waypoint_index_is_0(void) {
  gps_coordinates_t location = close_to_node_0;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(0, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_not_0(void) {
  gps_coordinates_t location = close_to_node_1;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_NOT_EQUAL(0, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_1(void) {
  gps_coordinates_t location = close_to_node_1;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(1, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_2(void) {
  gps_coordinates_t location = close_to_node_2;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(2, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_3(void) {
  gps_coordinates_t location = close_to_node_3;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(3, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_4(void) {
  gps_coordinates_t location = close_to_node_4;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(4, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_5(void) {
  gps_coordinates_t location = close_to_node_5;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(5, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_6(void) {
  gps_coordinates_t location = close_to_node_6;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(6, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_7(void) {
  gps_coordinates_t location = close_to_node_7;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(7, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_8(void) {
  gps_coordinates_t location = close_to_node_8;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(8, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_is_9(void) {
  gps_coordinates_t location = close_to_node_9;

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(9, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_location_top_left_of_garage(void) {
  gps_coordinates_t location = {37.3396749, -121.881445};

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(5, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_location_top_right_of_garage(void) {
  gps_coordinates_t location = {37.339951, -121.880771};

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(9, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_location_bottom_left_of_garage(void) {
  gps_coordinates_t location = {37.338671, -121.880708};

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(0, closest_waypoint);
}

void test_waypoint__closest_waypoint_index_location_bottom_right_of_garage(void) {
  gps_coordinates_t location = {37.339026, -121.880078};

  uint8_t closest_waypoint = 10;

  closest_waypoint = waypoint__find_closest_waypoint_index(location);

  TEST_ASSERT_EQUAL(7, closest_waypoint);
}

void test_waypoint__is_on_critical_node_path__current_node_0_destination_node_4(void) {
  gps_coordinates_t current_location = close_to_node_0;
  gps_coordinates_t final_destination = close_to_node_4;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_TRUE(on_critical_path);
}

void test_waypoint__is_on_critical_node_path__current_node_0_destination_node_5(void) {
  gps_coordinates_t current_location = close_to_node_0;
  gps_coordinates_t final_destination = close_to_node_5;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_TRUE(on_critical_path);
}

void test_waypoint__is_on_critical_node_path__current_node_1_destination_node_4(void) {
  gps_coordinates_t current_location = close_to_node_1;
  gps_coordinates_t final_destination = close_to_node_4;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_TRUE(on_critical_path);
}

void test_waypoint__is_on_critical_node_path__current_node_1_destination_node_5(void) {
  gps_coordinates_t current_location = close_to_node_1;
  gps_coordinates_t final_destination = close_to_node_5;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_TRUE(on_critical_path);
}

void test_waypoint__is_on_critical_node_path__current_node_4_destination_node_0(void) {
  gps_coordinates_t current_location = close_to_node_4;
  gps_coordinates_t final_destination = close_to_node_0;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_TRUE(on_critical_path);
}

void test_waypoint__is_on_critical_node_path__current_node_4_destination_node_1(void) {
  gps_coordinates_t current_location = close_to_node_4;
  gps_coordinates_t final_destination = close_to_node_1;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_TRUE(on_critical_path);
}

void test_waypoint__is_on_critical_node_path__current_node_5_destination_node_0(void) {
  gps_coordinates_t current_location = close_to_node_5;
  gps_coordinates_t final_destination = close_to_node_0;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_TRUE(on_critical_path);
}

void test_waypoint__is_on_critical_node_path__current_node_5_destination_node_1(void) {
  gps_coordinates_t current_location = close_to_node_5;
  gps_coordinates_t final_destination = close_to_node_1;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_TRUE(on_critical_path);
}

void test_waypoint__is_not_on_critical_node_path__current_node_0_destination_node_1(void) {
  gps_coordinates_t current_location = close_to_node_0;
  gps_coordinates_t final_destination = close_to_node_1;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_FALSE(on_critical_path);
}

void test_waypoint__is_not_on_critical_node_path__current_node_1_destination_node_1(void) {
  gps_coordinates_t current_location = close_to_node_1;
  gps_coordinates_t final_destination = close_to_node_1;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_FALSE(on_critical_path);
}

void test_waypoint__is_not_on_critical_node_path__current_node_6_destination_node_7(void) {
  gps_coordinates_t current_location = close_to_node_6;
  gps_coordinates_t final_destination = close_to_node_7;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_FALSE(on_critical_path);
}

void test_waypoint__is_not_on_critical_node_path__current_node_7_destination_node_5(void) {
  gps_coordinates_t current_location = close_to_node_6;
  gps_coordinates_t final_destination = close_to_node_7;
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);

  uint8_t current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  uint8_t final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  bool on_critical_path = waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                                             final_destination_closest_waypoint_index);

  TEST_ASSERT_FALSE(on_critical_path);
}

void test_waypoint__check_that_current_destination_travels_through_critical_path(void) {
  // check first location
  gps_coordinates_t current_location = {37.338993, -121.880692};
  gps_coordinates_t final_destination = {37.339666, -121.881364};
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);
  gps_coordinates_t current_destination = waypoint__update_current_destination();
  gps_coordinates_t expected_current_destination = waypoints[1];
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);

  // reached second critical node, check next node
  current_location.latitude = 37.338968;
  current_location.longitude = -121.880829;
  waypoint__update_current_location(current_location);
  current_destination = waypoint__update_current_destination();
  expected_current_destination = waypoints[2];
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);

  // did not yet reach third critical node
  current_location.latitude = 37.338992;
  current_location.longitude = -121.880838;
  waypoint__update_current_location(current_location);
  current_destination = waypoint__update_current_destination();
  expected_current_destination = waypoints[2];
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);

  // did reach third critical node, check to see if it wants to go to next node
  current_location.latitude = 37.339076;
  current_location.longitude = -121.880818;
  waypoint__update_current_location(current_location);
  current_destination = waypoint__update_current_destination();
  expected_current_destination = waypoints[3];
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);

  // did reach fourth critical node, check to see if it wants to go to next node
  current_location.latitude = 37.339354;
  current_location.longitude = -121.880877;
  waypoint__update_current_location(current_location);
  current_destination = waypoint__update_current_destination();
  expected_current_destination = waypoints[4];
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);

  // did reach fifth critical node, check to see if it wants to go to next node
  current_location.latitude = 37.339424;
  current_location.longitude = -121.881073;
  waypoint__update_current_location(current_location);
  current_destination = waypoint__update_current_destination();
  expected_current_destination = waypoints[5];
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);

  // did reach sixth critical node, check to see if it wants to go to final destination
  current_location.latitude = 37.339488;
  current_location.longitude = -121.881233;
  waypoint__update_current_location(current_location);
  current_destination = waypoint__update_current_destination();
  expected_current_destination = final_destination;
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);
}

void test_waypoint__check_that_current_destination_goes_straight_to_final_destination(void) {
  gps_coordinates_t current_location = {37.339745, -121.881044};
  gps_coordinates_t final_destination = {37.339872, -121.880766};
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);
  gps_coordinates_t current_destination = waypoint__update_current_destination();
  gps_coordinates_t expected_current_destination = final_destination;
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);
}

void test_waypoint__check_that_current_destination_stops_at_one_waypoint_before_final_destination(void) {
  gps_coordinates_t current_location = {37.339549, -121.8807933};
  gps_coordinates_t final_destination = {37.339431, -121.880372};
  waypoint__update_current_location(current_location);
  waypoint__update_final_destination(final_destination);
  gps_coordinates_t current_destination = waypoint__update_current_destination();
  gps_coordinates_t expected_current_destination = waypoints[9];
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);

  // at node 9 now
  waypoint__update_current_location(close_to_node_9);
  current_destination = waypoint__update_current_destination();
  expected_current_destination = final_destination;
  TEST_ASSERT_EQUAL(expected_current_destination.latitude, current_destination.latitude);
  TEST_ASSERT_EQUAL(expected_current_destination.longitude, current_destination.longitude);
}