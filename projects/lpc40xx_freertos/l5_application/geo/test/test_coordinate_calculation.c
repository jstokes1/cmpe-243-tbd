#include "unity.h"

#include <math.h>
#include <stdbool.h>

#include "coordinate_calculation.h"

typedef struct {
  float latitude;
  float longitude;
} gps_coordinates_t;

void test_coordinate_calculation__simple_coordinates_distance(void) {
  float distance = 0;

  gps_coordinates_t location_current = {0, 0};
  gps_coordinates_t location_destination = {3.0, 4.0};

  distance = coordinate_calculation__distance(location_current.latitude, location_current.longitude,
                                              location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(5.0, distance);
}

void test_coordinate_calculation__realistic_coordinates_distance(void) {
  float distance = 0;

  gps_coordinates_t location_current = {37.338966, -121.880890};
  gps_coordinates_t location_destination = {37.339096, -121.880820};

  distance = coordinate_calculation__distance(location_current.latitude, location_current.longitude,
                                              location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(0.000148, distance);
}

void test_coordinate_calculation__same_coordinates_distance(void) {
  float distance = 0;

  gps_coordinates_t location_current = {37.338966, -121.880890};
  gps_coordinates_t location_destination = {37.338966, -121.880890};

  distance = coordinate_calculation__distance(location_current.latitude, location_current.longitude,
                                              location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(0, distance);
}

void test_coordinate_calculation__close_coordinates_distance(void) {
  float distance = 0;

  gps_coordinates_t location_current = {37.338969, -121.880890};
  gps_coordinates_t location_destination = {37.338966, -121.880891};

  distance = coordinate_calculation__distance(location_current.latitude, location_current.longitude,
                                              location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(0.000006, distance);
}

void test_coordinate_calculation__zero_distance_case(void) {
  float distance = 12;

  gps_coordinates_t location_current = {37.338969, -121.880890};
  gps_coordinates_t location_destination = {0, 0};

  distance = coordinate_calculation__distance(location_current.latitude, location_current.longitude,
                                              location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(0, distance);
}

void test_coordinate_calculation__simple_coordinates_heading(void) {
  float heading = 0;

  gps_coordinates_t location_current = {0, 0};
  gps_coordinates_t location_destination = {1.0, sqrt(3.0)};

  heading = coordinate_calculation__heading(location_current.latitude, location_current.longitude,
                                            location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(30, heading);
}

void test_coordinate_calculation__negative_y_coordinates_heading(void) {
  float heading = 0;

  gps_coordinates_t location_current = {0, 0};
  gps_coordinates_t location_destination = {1.0, 0 - sqrt(3.0)};

  heading = coordinate_calculation__heading(location_current.latitude, location_current.longitude,
                                            location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(150, heading);
}

void test_coordinate_calculation__negative_x_coordinates_heading(void) {
  float heading = 0;

  gps_coordinates_t location_current = {0, 0};
  gps_coordinates_t location_destination = {0 - 1.0, sqrt(3.0)};

  heading = coordinate_calculation__heading(location_current.latitude, location_current.longitude,
                                            location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(330, heading);
}

void test_coordinate_calculation__negative_x_and_y_coordinates_heading(void) {
  float heading = 0;

  gps_coordinates_t location_current = {0, 0};
  gps_coordinates_t location_destination = {0 - 1.0, 0 - sqrt(3.0)};

  heading = coordinate_calculation__heading(location_current.latitude, location_current.longitude,
                                            location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_EQUAL(210, heading);
}

void test_coordinate_calculation__arrived_at_destination(void) {
  bool arrived = false;

  gps_coordinates_t location_current = {37.338969, -121.880890};
  gps_coordinates_t location_destination = {37.338966, -121.880891};

  arrived = coordinate_calculation__arrived(location_current.latitude, location_current.longitude,
                                            location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_TRUE(arrived);
}

void test_coordinate_calculation__not_arrived_at_destination(void) {
  bool arrived = true;

  gps_coordinates_t location_current = {37.338169, -121.880890};
  gps_coordinates_t location_destination = {37.338966, -121.880891};

  arrived = coordinate_calculation__arrived(location_current.latitude, location_current.longitude,
                                            location_destination.latitude, location_destination.longitude);
  TEST_ASSERT_FALSE(arrived);
}