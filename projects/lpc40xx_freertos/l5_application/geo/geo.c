#define _USE_MATH_DEFINES 1

#include "geo.h"
#include "can_bus.h"
#include "can_mia_configurations.h"
#include "lsm303_compass.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "waypoint.h"

static gps_coordinates_t geo_current_location = {0};
static gps_coordinates_t geo_current_destination = {0};
static gps_coordinates_t geo_final_location = {0};
static dbc_GPS_DESTINATION_LOCATION_s gps_final_destination_location = {0};
static lsm303_compass__data_s compass = {0};
static dbc_GEO_STATUS_s geo_status = {0U};

static dbc_GEO_CURRENT_LOCATION_DEBUG_s geo_location_debug = {0};
static dbc_GEO_CURRENT_DESTINATION_LOCATION_DEBUG_s geo_current_destination_location_debug = {0};

static float curr_latitude = 0;
static float curr_longitude = 0;

static bool gps_unlocked_or_no_destination_location(void) {
  return geo_current_destination.latitude == 0.0 || geo_current_destination.longitude == 0.0 || curr_latitude == 0.0 ||
         curr_longitude == 0.0;
}

static float convert_degrees_to_radians(float degrees) {
  const float degree_to_radians = M_PI / 180;
  float radians = degrees * degree_to_radians;
  return radians;
}

static float calculate_distance_in_meters(void) {
  const float radius_of_earth_km = 6378.137;
  float destination_latitude_radians = convert_degrees_to_radians(geo_current_destination.latitude);
  float current_latitude_radians = convert_degrees_to_radians(curr_latitude);
  float latitude_difference = destination_latitude_radians - current_latitude_radians;

  float longitude_difference = (geo_current_destination.longitude - curr_longitude) * (M_PI / 180);

  float haversine_angle = pow(sinf(latitude_difference / 2), 2) + cosf(current_latitude_radians) *
                                                                      cosf(destination_latitude_radians) *
                                                                      pow(sinf(longitude_difference / 2), 2);
  float distance = 2 * atan2f(sqrt(haversine_angle), sqrt(1 - haversine_angle));
  float distance_in_meters = radius_of_earth_km * distance * 1000;
  return distance_in_meters;
}

static float calculate_destination_heading(void) {
  float lat_difference = geo_current_destination.latitude - curr_latitude;
  float long_difference = geo_current_destination.longitude - curr_longitude;

  float destination_heading = atan(long_difference / lat_difference) * (180 / M_PI);
  // adjust for negative longitude
  if (lat_difference < 0) {
    destination_heading += (float)180.0;
  }
  // adjust for negative angle
  if (destination_heading < 0) {
    // Account for DBC GEO_STATUS message ranges
    destination_heading += (float)360.0;
  }

  return destination_heading;
}

void geo__process_messages(void) {
  // Receive all messages
  can__msg_t can_msg = {};
  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };
    dbc_decode_GPS_DESTINATION_LOCATION(&gps_final_destination_location, header, can_msg.data.bytes);
  }
  geo_final_location.latitude = gps_final_destination_location.GPS_DESTINATION_LOCATION_latitude;
  geo_final_location.longitude = gps_final_destination_location.GPS_DESTINATION_LOCATION_longitude;
  waypoint__update_final_destination(geo_final_location);
  // geo_current_destination = waypoint__update_current_destination();
}

void geo__manage_mia_10hz(void) {
  // We are in 10hz slot, so increment MIA counter by 100ms
  const uint32_t mia_increment_value = 100;

  if (dbc_service_mia_GPS_DESTINATION_LOCATION(&gps_final_destination_location, mia_increment_value)) {
    geo_current_destination.latitude = 0;
    geo_current_destination.longitude = 0;
    curr_latitude = 0;
    curr_longitude = 0;
  }
}

void geo__update_destination_heading(void) {
  if (gps_unlocked_or_no_destination_location()) {
    geo_status.GEO_STATUS_destination_heading = 0;
  } else {
    geo_status.GEO_STATUS_destination_heading = (uint16_t)calculate_destination_heading();
  }
}

void geo__update_distance_to_destination(void) {
  if (gps_unlocked_or_no_destination_location()) {
    geo_status.GEO_STATUS_distance_to_destination = 0;
  } else {
    geo_status.GEO_STATUS_distance_to_destination = calculate_distance_in_meters();
  }
}

void geo__update_compass_heading(void) {
  // TODO: handle compass i2c read byte fail
  compass = lsm303_compass__get_data();
  float current_heading = compass.degree;
  geo_status.GEO_STATUS_compass_heading = (uint16_t)current_heading;
}

void geo__update_current_coordinates(float latitude, float longitude) {
  geo_location_debug.GEO_CURRENT_LOCATION_DEBUG_latitude = latitude;
  geo_location_debug.GEO_CURRENT_LOCATION_DEBUG_longitude = longitude;
  curr_latitude = latitude;
  curr_longitude = longitude;
  geo_current_location.latitude = latitude;
  geo_current_location.longitude = longitude;
  waypoint__update_current_location(geo_current_location);
}

void geo__update_current_destination(void) {
  geo_current_destination = waypoint__update_current_destination();
  geo_current_destination_location_debug.GEO_CURRENT_DESTINATION_LOCATION_DEBUG_latitude =
      geo_current_destination.latitude;
  geo_current_destination_location_debug.GEO_CURRENT_DESTINATION_LOCATION_DEBUG_longitude =
      geo_current_destination.longitude;
}

dbc_GEO_STATUS_s geo__get_geo_status(void) { return geo_status; }

dbc_GEO_CURRENT_LOCATION_DEBUG_s geo__get_geo_current_location_debug(void) { return geo_location_debug; }

float geo__get_current_latitude(void) { return curr_latitude; }

float geo__get_current_longitude(void) { return curr_longitude; }

void geo__set_gps_destination_location(dbc_GPS_DESTINATION_LOCATION_s new_location) {
  gps_final_destination_location = new_location;
}

dbc_GPS_DESTINATION_LOCATION_s geo__get_gps_destination_location(void) { return gps_final_destination_location; }

dbc_GEO_CURRENT_DESTINATION_LOCATION_DEBUG_s geo__get_gps_current_destination_location_debug(void) {
  return geo_current_destination_location_debug;
}