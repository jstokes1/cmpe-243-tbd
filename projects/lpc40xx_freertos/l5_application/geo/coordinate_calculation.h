#pragma once

#include <stdbool.h>

float coordinate_calculation__distance(float latitude_current, float longitude_current, float latitude_destination,
                                       float longitude_destination);

float coordinate_calculation__heading(float latitude_current, float longitude_current, float latitude_destination,
                                      float longitude_destination);

bool coordinate_calculation__arrived(float latitude_current, float longitude_current, float latitude_destination,
                                     float longitude_destination);