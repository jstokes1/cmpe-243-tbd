#include "waypoint.h"

#include "coordinate_calculation.h"

#define CRITICAL_NODE_0 0
#define CRITICAL_NODE_1 1
#define CRITICAL_NODE_2 4
#define CRITICAL_NODE_3 5

static const gps_coordinates_t waypoints[] = {
    {37.338966, -121.880890}, {37.339096, -121.880820}, {37.339228, -121.880781}, {37.339358, -121.880887},
    {37.339415, -121.881075}, {37.339558, -121.881210}, {37.339235, -121.880628}, {37.339345, -121.880416},
    {37.339412, -121.880750}, {37.339520, -121.880498}};

static const int max_waypoints = 10;

static gps_coordinates_t current_location = {0};

static gps_coordinates_t final_destination = {0};

static bool critical_path_flag = false;

static uint8_t current_critical_node = 0;

static uint8_t final_critical_node = 0;

uint8_t waypoint__find_closest_waypoint_index(gps_coordinates_t location) {
  uint8_t closest_node_index = 10;
  float closest_distance = 100;
  float distance_between_location_and_node = 0;

  for (int index = 0; index < max_waypoints; index++) {
    distance_between_location_and_node = coordinate_calculation__distance(
        location.latitude, location.longitude, waypoints[index].latitude, waypoints[index].longitude);
    if (distance_between_location_and_node < closest_distance) {
      closest_distance = distance_between_location_and_node;
      closest_node_index = index;
    }
  }
  return closest_node_index;
}

void waypoint__update_final_destination(gps_coordinates_t new_final_destination) {
  if (final_destination.latitude != new_final_destination.latitude &&
      final_destination.longitude != new_final_destination.longitude) {
    critical_path_flag = false;
  }
  final_destination = new_final_destination;
}

void waypoint__update_current_location(gps_coordinates_t new_current_location) {
  if (new_current_location.latitude == 0 && new_current_location.longitude == 0) {
    critical_path_flag = false;
  }
  current_location = new_current_location;
}

bool waypoint__check_critical_node_path(uint8_t current_location_closest_waypoint_index,
                                        uint8_t final_destination_closest_waypoint_index) {

  bool current_position_is_below_roundabout = current_location_closest_waypoint_index == CRITICAL_NODE_0 ||
                                              current_location_closest_waypoint_index == CRITICAL_NODE_1;
  bool current_position_is_above_roundabout = current_location_closest_waypoint_index == CRITICAL_NODE_2 ||
                                              current_location_closest_waypoint_index == CRITICAL_NODE_3;

  bool destination_is_below_roundabout = final_destination_closest_waypoint_index == CRITICAL_NODE_0 ||
                                         final_destination_closest_waypoint_index == CRITICAL_NODE_1;
  bool destination_is_above_roundabout = final_destination_closest_waypoint_index == CRITICAL_NODE_2 ||
                                         final_destination_closest_waypoint_index == CRITICAL_NODE_3;

  if ((current_position_is_below_roundabout && destination_is_above_roundabout) ||
      (current_position_is_above_roundabout && destination_is_below_roundabout)) {
    // go on critical path
    critical_path_flag = true;
    current_critical_node = current_location_closest_waypoint_index;
    final_critical_node = final_destination_closest_waypoint_index;
    return true;
  }
  critical_path_flag = false;
  return false;
}

gps_coordinates_t waypoint__update_current_destination(void) {
  uint8_t current_location_closest_waypoint_index = 0;
  uint8_t final_destination_closest_waypoint_index = 0;

  float waypoint_dist_to_final_loc = 0;
  float current_location_dist_to_final_loc = 0;

  if (current_location.latitude == 0 && current_location.longitude == 0) {
    return final_destination;
  }

  // critical path travel
  if (critical_path_flag) {
    // have not arrived at next critical node
    if (!coordinate_calculation__arrived(current_location.latitude, current_location.longitude,
                                         waypoints[current_critical_node].latitude,
                                         waypoints[current_critical_node].longitude)) {
      return waypoints[current_critical_node];
    }
    // have arrived at critical node, return next node
    else {
      if (current_critical_node > final_critical_node) {
        current_critical_node--;
        return waypoints[current_critical_node];
      } else if (current_critical_node < final_critical_node) {
        current_critical_node++;
        return waypoints[current_critical_node];
      }
      // arrived at last critical node, continue to final destination
      else {
        critical_path_flag = false;
        return final_destination;
      }
    }
  }

  current_location_closest_waypoint_index = waypoint__find_closest_waypoint_index(current_location);
  final_destination_closest_waypoint_index = waypoint__find_closest_waypoint_index(final_destination);

  // check to see if on critical path
  if (waypoint__check_critical_node_path(current_location_closest_waypoint_index,
                                         final_destination_closest_waypoint_index)) {
    return waypoints[current_location_closest_waypoint_index];
  }

  // calculate distances to waypoint and final location
  waypoint_dist_to_final_loc =
      coordinate_calculation__distance(waypoints[current_location_closest_waypoint_index].latitude,
                                       waypoints[current_location_closest_waypoint_index].longitude,
                                       final_destination.latitude, final_destination.longitude);
  current_location_dist_to_final_loc = coordinate_calculation__distance(
      current_location.latitude, current_location.longitude, final_destination.latitude, final_destination.longitude);

  // if not yet arrived at node and if node is closer to destination than car
  bool arrived_at_waypoint =
      coordinate_calculation__arrived(current_location.latitude, current_location.longitude,
                                      waypoints[current_location_closest_waypoint_index].latitude,
                                      waypoints[current_location_closest_waypoint_index].longitude);

  if (!arrived_at_waypoint && waypoint_dist_to_final_loc < current_location_dist_to_final_loc) {
    return waypoints[current_location_closest_waypoint_index];
  } else {
    return final_destination;
  }
}