#pragma once

#include "project.h"

void geo__process_messages(void);

void geo__manage_mia_10hz(void);

void geo__update_destination_heading(void);

void geo__update_distance_to_destination(void);

void geo__update_compass_heading(void);

void geo__update_current_coordinates(float latitude, float longitude);

void geo__update_current_destination(void);

dbc_GEO_STATUS_s geo__get_geo_status(void);

dbc_GEO_CURRENT_LOCATION_DEBUG_s geo__get_geo_current_location_debug(void);

float geo__get_current_latitude(void);
float geo__get_current_longitude(void);
void geo__set_gps_destination_location(dbc_GPS_DESTINATION_LOCATION_s new_location);
dbc_GPS_DESTINATION_LOCATION_s geo__get_gps_destination_location(void);
dbc_GEO_CURRENT_DESTINATION_LOCATION_DEBUG_s geo__get_gps_current_destination_location_debug(void);