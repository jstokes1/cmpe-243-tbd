#include "coordinate_calculation.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define M_PI 3.14159265358979323846

float coordinate_calculation__distance(float latitude_current, float longitude_current, float latitude_destination,
                                       float longitude_destination) {
  if (abs(latitude_destination - (float)0.0) < (float)0.00000000001 &&
      abs(longitude_destination - (float)0.0) < (float)0.00000000001) {
    return 0;
  }
  float lat_difference = latitude_destination - latitude_current;
  float long_difference = longitude_destination - longitude_current;
  float lat_sq = lat_difference * lat_difference;
  float long_sq = long_difference * long_difference;

  float distance = sqrt(lat_sq + long_sq);

  return distance;
}

float coordinate_calculation__heading(float latitude_current, float longitude_current, float latitude_destination,
                                      float longitude_destination) {
  if (abs(latitude_destination - (float)0.0) < (float)0.00000000001 &&
      abs(longitude_destination - (float)0.0) < (float)0.00000000001) {
    return 0;
  }

  float lat_difference = latitude_destination - latitude_current;
  float long_difference = longitude_destination - longitude_current;

  float heading = atan(lat_difference / long_difference) * (180 / M_PI);
  // adjust for negative longitude
  if (long_difference < 0) {
    heading += (float)180.0;
  }
  // adjust for negative angle
  if (heading < 0) {
    // Account for DBC GEO_STATUS message ranges
    heading += (float)360.0;
  }

  return heading;
}

bool coordinate_calculation__arrived(float latitude_current, float longitude_current, float latitude_destination,
                                     float longitude_destination) {
  float margin_of_error = 0.00007;

  float distance = coordinate_calculation__distance(latitude_current, longitude_current, latitude_destination,
                                                    longitude_destination);

  return distance < margin_of_error;
}