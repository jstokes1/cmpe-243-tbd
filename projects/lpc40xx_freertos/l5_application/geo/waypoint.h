#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct {
  float latitude;
  float longitude;
} gps_coordinates_t;

uint8_t waypoint__find_closest_waypoint_index(gps_coordinates_t location);

void waypoint__update_final_destination(gps_coordinates_t new_final_destination);

void waypoint__update_current_location(gps_coordinates_t new_current_location);

bool waypoint__check_critical_node_path(uint8_t current_location_closest_waypoint_index,
                                        uint8_t final_destination_closest_waypoint_index);

gps_coordinates_t waypoint__update_current_destination(void);