// gps.h
#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "project.h"

typedef struct {
  float latitude;
  float longitude;
} gps_coordinates_t;

void gps__init(void);
bool gps__run_once(void);
void gps__send_cmd(void);

gps_coordinates_t gps__get_coordinates(void);

dbc_GEO_DEBUG_MSG_s gps__get_gps_debug(void);