#pragma once

void uart_init_nmea_reset(void);

void uart_init_nmea_gpgga(void);

void uart_init_datum(void);