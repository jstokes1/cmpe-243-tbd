#include "gps_config.h"

#include "uart.h"

void gps_config__init_nmea_reset(void) {
  uint8_t message_data[100] = {0xA0, 0xA1, 0};
  uint8_t cs = 8;
  uint16_t length = 9;
  message_data[5] = 0;
  message_data[6] = 0;
  message_data[7] = 0;
  message_data[8] = 0;
  message_data[9] = 0;
  message_data[10] = 0;
  message_data[11] = 0;
  message_data[12] = 0;
  for (int index = 5; index < length + 4; index++) {
    cs = cs ^ message_data[index];
  }
  message_data[2] = length / 256;
  message_data[3] = length % 256;
  message_data[4] = 8;
  message_data[4 + length] = cs;
  message_data[5 + length] = 0x0D;
  message_data[6 + length] = 0x0A;

  for (int i = 0; i < 7 + length; i++) {
    uart__polled_put(UART__1, message_data[i]);
  }
}

void gps_config__init_nmea_gpgga(void) {
  uint8_t message_data[100] = {0xA0, 0xA1, 0};
  uint8_t cs = 8;
  uint16_t length = 9;
  message_data[5] = 1;
  message_data[6] = 0;
  message_data[7] = 0;
  message_data[8] = 0;
  message_data[9] = 0;
  message_data[10] = 0;
  message_data[11] = 0;
  message_data[12] = 0;
  for (int index = 5; index < length + 4; index++) {
    cs = cs ^ message_data[index];
  }
  message_data[2] = length / 256;
  message_data[3] = length % 256;
  message_data[4] = 8;
  message_data[4 + length] = cs;
  message_data[5 + length] = 0x0D;
  message_data[6 + length] = 0x0A;

  for (int i = 0; i < 7 + length; i++) {
    uart__polled_put(UART__1, message_data[i]);
  }
}

void gps_config__init_datum(void) {
  uint8_t message_data[100] = {0xA0, 0xA1, 0};
  uint8_t cs = 41;
  uint16_t length = 19;
  message_data[5] = 0;
  message_data[6] = 134;
  message_data[7] = 6;
  message_data[8] = 255;
  message_data[9] = 250;
  message_data[10] = 0;
  message_data[11] = 159;
  message_data[12] = 0;
  message_data[13] = 175;
  message_data[14] = 0x00;
  message_data[15] = 0x7D;
  message_data[16] = 0x38;
  message_data[17] = 0x40;
  message_data[18] = 0x01;
  message_data[19] = 0x2D;
  message_data[20] = 0xEC;
  message_data[21] = 0xE6;
  message_data[22] = 0;

  for (int index = 5; index < length + 4; index++) {
    cs = cs ^ message_data[index];
  }
  message_data[2] = length / 256;
  message_data[3] = length % 256;
  message_data[4] = 41;
  message_data[4 + length] = cs;
  message_data[5 + length] = 0x0D;
  message_data[6 + length] = 0x0A;

  for (int i = 0; i < 7 + length; i++) {
    uart__polled_put(UART__1, message_data[i]);
  }
}