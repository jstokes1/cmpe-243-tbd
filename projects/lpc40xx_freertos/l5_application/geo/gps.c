// gps.c
#include "gps.h"

#include "gps_config.h"

// GPS module dependency
#include "uart.h"

#include "clock.h" // needed for UART initialization

#include "gpio.h"

#include "project.h"

// Change this according to which UART you plan to use
static const uart_e gps_uart = UART__1;

// Space for the line buffer, and the line buffer data structure instance
static char gps_string[89];
static int gps_string_index;

static gps_coordinates_t parsed_coordinates;

static dbc_GEO_DEBUG_MSG_s geo_debug = {0U};

const char start_symbol = '$';

static void gps__transfer_data_from_uart(void) {
  char byte;
  const uint32_t zero_timeout = 0;

  // uart_init_nmea_1();

  while (uart__get(gps_uart, &byte, zero_timeout)) {
    // printf("%c", byte);
    if (byte == start_symbol || gps_string_index >= 89) {
      gps_string_index = 0;
    }
    gps_string[gps_string_index] = byte;
    gps_string_index++;
  }
}

static float get_true_coordinate(float coordinate) {
  float true_coordinate = 0;
  float coordinate_minutes = 0;

  true_coordinate = (int)coordinate / 100;
  coordinate_minutes = coordinate - ((int)coordinate / 100) * 100;
  return true_coordinate + coordinate_minutes / 60;
}

static bool gps__parse_coordinates_from_line(void) {
  char gps_final_string[89];
  float time = 0;
  float latitude = 0;
  char latitude_dir;
  float longitude = 0;
  char longitude_dir;
  bool gps_status = false;
  double satellites = 0;

  bool got_latitude = false;
  bool got_longitude = false;

  const char *delim = ",";
  static char *token;

  for (int i = 0; i < 89; i++) {
    gps_final_string[i] = gps_string[i];
  }

  token = strtok(gps_final_string, delim);
  if (!strcmp(token, "$GPGGA")) {
    parsed_coordinates.latitude = 0;
    parsed_coordinates.longitude = 0;

    // get time
    token = strtok(NULL, delim);
    if (token != NULL)
      time = atof(token);

    // get latitude
    token = strtok(NULL, delim);
    if (token != NULL) {
      latitude = atof(token);
      got_latitude = true;
    }

    // get latitude direction N/S
    token = strtok(NULL, delim);
    if (token != NULL) {
      latitude_dir = *token;
      if (!strcmp(&latitude_dir, "S"))
        latitude = 0 - latitude;
    }

    // get longitude
    token = strtok(NULL, delim);
    if (token != NULL) {
      longitude = atof(token);
      got_longitude = true;
    }

    // get longitude direction E/W
    token = strtok(NULL, delim);
    if (token != NULL) {
      longitude_dir = *token;
      if (strcmp(&longitude_dir, "W"))
        longitude = 0 - longitude;
    }

    // quality indicator
    token = strtok(NULL, delim);
    if (token != NULL) {
      if (!strcmp(token, "0"))
        gps_status = true;
    }

    // number of satellites
    token = strtok(NULL, delim);
    if (token != NULL) {
      satellites = atof(token);
    }
  }

  if (got_latitude && got_longitude) {
    parsed_coordinates.latitude = get_true_coordinate(latitude);
    parsed_coordinates.longitude = get_true_coordinate(longitude);

    if (gps_status)
      geo_debug.GEO_DEBUG_MSG_lock_status = 1;
    else
      geo_debug.GEO_DEBUG_MSG_lock_status = 0;

    geo_debug.GEO_DEBUG_MSG_num_satellites = satellites;

    got_latitude = false;
    got_longitude = false;
    return true;
  }
  return false;
}

static void gps__uart_init(void) {
  gpio__construct_with_function(GPIO__PORT_0, 15, GPIO__FUNCTION_1); // P0.15 - Uart-1 Tx
  gpio__construct_with_function(GPIO__PORT_0, 16, GPIO__FUNCTION_1); // P0.16 - Uart-1 RX

  uart__init(gps_uart, clock__get_peripheral_clock_hz(), 9600);

  QueueHandle_t rxq_handle = xQueueCreate(200, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(200, sizeof(char)); // We don't send anything to the GPS
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
}

void gps__init(void) {
  gps__uart_init();
  gps_string_index = 0;
}

bool gps__run_once(void) {
  gps__transfer_data_from_uart();
  return gps__parse_coordinates_from_line();
}

void gps__send_cmd(void) {
  // gps_config__init_nmea_reset();
  gps_config__init_nmea_gpgga();
  //  gps_config__init_datum();
}

gps_coordinates_t gps__get_coordinates(void) { return parsed_coordinates; }

dbc_GEO_DEBUG_MSG_s gps__get_gps_debug(void) { return geo_debug; }