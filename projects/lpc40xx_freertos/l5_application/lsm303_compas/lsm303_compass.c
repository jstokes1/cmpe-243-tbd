
#include "lsm303_compass.h"
#include "i2c.h"
#include <math.h>
#include <stdio.h>

static const i2c_e lsm303_compass__sensor_bus = I2C__2;
static const uint8_t lsm303_compass__address = 0x3C;

typedef enum {
  lsm303_compass__memory_axis_base = 0x03,
  lsm303_compass__CRA_REG_M = 0x00,
  lsm303_compass__MR_REG_M = 0x02,
  lsm303_compass__IR_REG_M = 0x0A,
  // lsm303_compass__CRB_REG_M = 0x01, // maybe use this to change gain setting
} lsm303_compass__memory_e;

bool lsm303_compass__init(void) {
  const uint8_t active_mode_with_220Hz = 0x1C;
  const uint8_t continuous_conversion = 0x00;
  i2c__write_single(lsm303_compass__sensor_bus, lsm303_compass__address, lsm303_compass__CRA_REG_M,
                    active_mode_with_220Hz);
  i2c__write_single(lsm303_compass__sensor_bus, lsm303_compass__address, lsm303_compass__MR_REG_M,
                    continuous_conversion);
  const uint8_t IR_REG_M_expected_value = 0x48;
  // i2c__write_single(lsm303_compass__sensor_bus, lsm303_compass__address, lsm303_compass__CRB_REG_M, 0x20);

  const uint8_t IR_REG_M_actual_value =
      i2c__read_single(lsm303_compass__sensor_bus, lsm303_compass__address, lsm303_compass__IR_REG_M);
  return (IR_REG_M_expected_value == IR_REG_M_actual_value);
}

static double calibrated_vales(lsm303_compass__data_s compass_data) {
  lsm303_compass__data_s calibrated_data = {0};

  float X_offset, Y_offset, Z_offset;

  // X_offset = (compass_data.x / 1100.00 * 100) - 9.645741;  // axis raw value devided by gauss value - bias
  // Y_offset = (compass_data.y / 1100.00 * 100) + 20.852170; //
  // Z_offset = (compass_data.z / 980.0 * 100) + 30.683837;   // Z-axis has a different gauss value

  // calibrated_data.x = 0.825814 * X_offset + 0.011106 * Y_offset + 0.021089 * Z_offset;
  // calibrated_data.y = 0.011106 * X_offset + 0.815562 * Y_offset + 0.005519 * Z_offset; //
  // calibrated_data.z = 0.021089 * X_offset + 0.005519 * Y_offset + 0.796532 * Z_offset; //

  // X_offset = compass_data.x;
  // Y_offset = compass_data.y;
  // Z_offset = compass_data.z;

  X_offset = (compass_data.x / 1100.00 * 100) - 10.607740; // axis raw value devided by gauss value - bias
  Y_offset = (compass_data.y / 1100.00 * 100) + 23.867664; //
  Z_offset = (compass_data.z / 980.0 * 100) + 29.267481;   // Z-axis has a different gauss value

  X_offset = 0.759463 * X_offset + 0.019219 * Y_offset + 0.004519 * Z_offset; // value of 3x3 matrix from magneto
  Y_offset = 0.019219 * X_offset + 0.760030 * Y_offset + 0.003038 * Z_offset; //
  Z_offset = 0.004519 * X_offset + 0.003038 * Y_offset + 0.738498 * Z_offset; //

  return atan2(X_offset, Y_offset) * 180 / M_PI;
}

lsm303_compass__data_s lsm303_compass__get_data(void) {
  lsm303_compass__data_s compass_data = {0};
  uint8_t raw_data[6] = {0};
  i2c__read_slave_data(lsm303_compass__sensor_bus, lsm303_compass__address, lsm303_compass__memory_axis_base, raw_data,
                       sizeof(raw_data));
  // First byte is MSB then LSB
  compass_data.x = ((uint16_t)raw_data[0] << 8) | raw_data[1];
  compass_data.z = ((uint16_t)raw_data[2] << 8) | raw_data[3];
  compass_data.y = ((uint16_t)raw_data[4] << 8) | raw_data[5];

  // double total_vector =
  //     sqrt((compass_data.x * compass_data.x) + (compass_data.y * compass_data.y) + (compass_data.z *
  //     compass_data.z));
  // double pitch = asin(compass_data.x / total_vector) * 180 / M_PI; // Calculate the pitch angle
  // double roll = -asin(compass_data.y / total_vector) * 180 / M_PI;

  // double tilt_x_un =
  //     compass_data.x * cos(pitch) + compass_data.y * sin(roll) * sin(pitch) - compass_data.z * cos(roll) *
  //     sin(pitch);
  // double tilt_y_un = compass_data.y * cos(roll) + compass_data.z * sin(roll);

  // double result = atan2(tilt_y_un, tilt_x_un);
  double result = calibrated_vales(compass_data);

  if (result < 0) {
    result += 360;
  }

  compass_data.degree = (int)result;
  return compass_data;
}