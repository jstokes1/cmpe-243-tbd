#pragma once

#include <stdbool.h>
#include <stdint.h>

// Data for x-axis, y-axis, z-axis and degrees
typedef struct {
  int16_t x, y, z;
  int16_t degree;
} lsm303_compass__data_s;

bool lsm303_compass__init(void);

/// @returns data of all 3-axis data value and total degrees
lsm303_compass__data_s lsm303_compass__get_data(void);
