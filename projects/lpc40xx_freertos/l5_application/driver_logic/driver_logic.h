#pragma once

#include "project.h"

#include "can_bus.h"
#include "gpio.h"

typedef enum {
  START,
  LICENSE_PLATE,
  MAIN_MENU_1,
  MAIN_MENU_2,
  MAIN_MENU_3,
  MAIN_MENU_4,
  MAIN_MENU_5,
  SENSOR_DATA,
  GEO_STATUS,
  GPS_INFO,
  LOCATION_INFO,
  MOTOR_COMMANDS
} driver_logic__debug_menu_states;

typedef enum { SW3, SW2, SW1, SW0 } driver_logic__pressed_key;

void driver_logic__init(void);
void driver_logic__process_messages(void);
void driver_logic__manage_mia_10hz(void);
void driver_logic__manage_debug_menu_10hz(uint32_t callback_count);
void driver_logic__process_geo_status(uint32_t callback_count);
void driver_logic__update_leds(uint32_t callback_count);

dbc_DRIVER_TO_MOTOR_CMD_s driver_logic__get_motor_command(void);

void obstacle_detection_leds_init(void);
void external_leds_init(void);
void lcd_debug_menu_init(void);

void set_sensor_data(dbc_SENSOR_TO_DRIVER_SONARS_s data);
void set_geo_status(dbc_GEO_STATUS_s current_geo_status);
void update_gps_lock_led(void);
void update_app_connected_led(void);
void update_taillights(uint32_t callback_count);
void drive_to_destination(void);

bool validate_key_press(bool sw3_value, bool sw2_value, bool sw1_value, bool sw0_value);
void update_pressed_key(bool sw3_value, bool sw2_value, bool sw1_value, bool sw0_value);
void update_key_pressed_on_key_unpressed(bool sw3_value, bool sw2_value, bool sw1_value, bool sw0_value);
void update_debug_menu_state(void);
