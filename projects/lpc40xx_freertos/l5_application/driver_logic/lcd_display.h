#pragma once

#include <stdbool.h>
#include <stdint.h>

void lcd_display__set_display_menu_debug_changed(bool value);
void display_start(bool sensor_mia, bool geo_mia, bool motor_mia);
void display_license_plate(void);
void display_main_menu_1(void);
void display_main_menu_2(void);
void display_main_menu_3(void);
void display_main_menu_4(void);
void display_main_menu_5(void);
void display_sensor_data(uint32_t callback_count, uint8_t front_left, uint8_t front_middle, uint8_t front_right,
                         uint8_t back);
void display_geo_status(uint32_t callback_count, uint16_t compass_heading, uint16_t destination_heading,
                        float distance_to_destination);
void display_gps_info(uint32_t callback_count, uint8_t lock_status, uint8_t num_satellites);
void display_location_info(uint32_t callback_count, float current_latitude, float current_longitude,
                           float destination_latitude, float destination_longitude);
void display_motor_commands(uint32_t callback_count, int16_t motor_command_steer, uint8_t motor_command_speed,
                            int16_t motor_debug_steer, uint8_t motor_debug_speed, uint16_t rpm);