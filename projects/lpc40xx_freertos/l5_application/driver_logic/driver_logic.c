#include "driver_logic.h"
#include "can_bus.h"
#include "can_mia_configurations.h"

#include "i2c_lcd.h"
#include "lcd_display.h"
#include "led_ring.h"
#include <stdio.h>

static const gpio_s front_left_obstacle_led = {.port_number = 0, .pin_number = 7};
static const gpio_s front_middle_obstacle_led = {.port_number = 0, .pin_number = 9};
static const gpio_s front_right_obstacle_led = {.port_number = 0, .pin_number = 25};
static const gpio_s back_obstacle_led = {.port_number = 1, .pin_number = 30};

static const gpio_s headlight_left = {.port_number = 2, .pin_number = 1};
static const gpio_s headlight_right = {.port_number = 2, .pin_number = 4};
static const gpio_s taillight_left = {.port_number = 2, .pin_number = 6};
static const gpio_s taillight_right = {.port_number = 2, .pin_number = 8};

static const gpio_s sw3 = {.port_number = 0, .pin_number = 29};
static const gpio_s sw2 = {.port_number = 0, .pin_number = 30};
static const gpio_s sw1 = {.port_number = 1, .pin_number = 15};
static const gpio_s sw0 = {.port_number = 1, .pin_number = 19};
static const gpio_s gps_lock_led = {.port_number = 1, .pin_number = 23};
static const gpio_s app_connected_led = {.port_number = 1, .pin_number = 29};

static const uint8_t no_obstacle = 255;
static const uint8_t ambiguous_obstacle = 140;
static const uint8_t reverse_until_obstacle_threshold = 60;
static const uint8_t start_deflection = 120;
static const uint8_t close_obstacle = 60;
static const uint8_t emergency_stop_threshold = 30;

static const uint8_t idle_DO_NOT_USE = 0;
static const uint8_t reverse_fast = 1;
static const uint8_t reverse_slow = 4;
static const uint8_t motor_stopped = 5;
static const uint8_t slow_motor_speed = 6;
static const uint8_t ambiguous_obstacle_speed = 30;
static const uint8_t max_motor_speed = 31;

typedef enum {
  LEFT = 0x0,
  CENTER,
  RIGHT,
} obstacle_closest_e;

static const uint8_t ESTOP_CHECK_DURATION = 140; // * 10ms

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

static const int max_motor_steer_left = -180;
static const int max_motor_steer_right = 179;

static const float distance_slowdown_threshold = 15;
static const float distance_stop_threshold = 5;

static const uint32_t led_ring_spi_clock_mhz = 15;

// Messages sent by DRIVER
static dbc_DRIVER_TO_MOTOR_CMD_s motor_command = {0};
// Critical messages for DRIVER node
static dbc_SENSOR_TO_DRIVER_SONARS_s sensor_data = {0};
static dbc_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_s wheel_encoder_data = {0};
static dbc_GEO_STATUS_s current_geo_status = {0};
// Debug messages
static dbc_GPS_DESTINATION_LOCATION_s gps_destination_location = {0};
static dbc_SENSOR_TO_DRIVER_APP_CONNECTION_MSG_s app_connection_msg = {0};
static dbc_GEO_DEBUG_MSG_s geo_debug_msg = {0};
static dbc_GEO_CURRENT_LOCATION_DEBUG_s geo_current_location_debug = {0};
static dbc_MOTOR_DEBUG_MSG_s motor_debug_msg = {0};

static bool sensor_mia = false;
static bool geo_mia = false;
static bool motor_mia = false;

static driver_logic__debug_menu_states debug_menu_state = START;
static bool key_pressed = false;
static driver_logic__pressed_key pressed_key = SW3;

static void update_obstacle_leds(dbc_SENSOR_TO_DRIVER_SONARS_s *s_data, uint32_t callback_count) {
  // uint8_t *leds = {&left, &middle, &right, &back};
  uint8_t leds[4] = {s_data->SENSOR_TO_DRIVER_SONARS_front_left, s_data->SENSOR_TO_DRIVER_SONARS_front_middle,
                     s_data->SENSOR_TO_DRIVER_SONARS_front_right, s_data->SENSOR_TO_DRIVER_SONARS_back};
  gpio_s led_gpios[4] = {front_left_obstacle_led, front_middle_obstacle_led, front_right_obstacle_led,
                         back_obstacle_led};
  for (int i = 0; i < 4; i++) {
    if (leds[i] == no_obstacle) {
      gpio__reset(led_gpios[i]);
    } else if (leds[i] > ambiguous_obstacle) {
      (callback_count % 12 == 0) ? gpio__set(led_gpios[i]) : gpio__reset(led_gpios[i]);
    } else if (leds[i] > close_obstacle) {
      (callback_count % 8 >= 4) ? gpio__set(led_gpios[i]) : gpio__reset(led_gpios[i]);
    } else if (leds[i] > emergency_stop_threshold) {
      (callback_count % 2 >= 1) ? gpio__set(led_gpios[i]) : gpio__reset(led_gpios[i]);
    } else {
      gpio__set(led_gpios[i]);
    }
  }
}

void driver_logic__init(void) {
  obstacle_detection_leds_init();
  external_leds_init();
  lcd_debug_menu_init();
  i2c_lcd__init();
  init_led_ring(led_ring_spi_clock_mhz, distance_stop_threshold);
}

void driver_logic__process_messages(void) {
  can__msg_t can_msg = {};

  // Receive all messages
  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };
    if (dbc_decode_SENSOR_TO_DRIVER_SONARS(&sensor_data, header, can_msg.data.bytes)) {
      sensor_mia = false;
    }
    if (dbc_decode_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG(&wheel_encoder_data, header, can_msg.data.bytes)) {
      sensor_mia = false;
    }
    if (dbc_decode_GPS_DESTINATION_LOCATION(&gps_destination_location, header, can_msg.data.bytes)) {
      sensor_mia = false;
    }
    if (dbc_decode_SENSOR_TO_DRIVER_APP_CONNECTION_MSG(&app_connection_msg, header, can_msg.data.bytes)) {
      sensor_mia = false;
    }
    if (dbc_decode_GEO_STATUS(&current_geo_status, header, can_msg.data.bytes)) {
      geo_mia = false;
    }
    if (dbc_decode_GEO_DEBUG_MSG(&geo_debug_msg, header, can_msg.data.bytes)) {
      geo_mia = false;
    }
    if (dbc_decode_GEO_CURRENT_LOCATION_DEBUG(&geo_current_location_debug, header, can_msg.data.bytes)) {
      geo_mia = false;
    }
    if (dbc_decode_MOTOR_DEBUG_MSG(&motor_debug_msg, header, can_msg.data.bytes)) {
      motor_mia = false;
    }
  }
}

void driver_logic__manage_mia_10hz(void) {
  // We are in 10hz slot, so increment MIA counter by 100ms
  const uint32_t mia_increment_value = 100;

  if (dbc_service_mia_SENSOR_TO_DRIVER_SONARS(&sensor_data, mia_increment_value)) {
    sensor_mia = true;
  }
  if (dbc_service_mia_SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG(&wheel_encoder_data, mia_increment_value)) {
    sensor_mia = true;
  }
  if (dbc_service_mia_GPS_DESTINATION_LOCATION(&gps_destination_location, mia_increment_value)) {
    sensor_mia = true;
  }
  // NOTE: APP CONNECTION message is only sent periodically if APP is connected (no need to set sensor_mia)
  dbc_service_mia_SENSOR_TO_DRIVER_APP_CONNECTION_MSG(&app_connection_msg, mia_increment_value);
  if (dbc_service_mia_GEO_STATUS(&current_geo_status, mia_increment_value)) {
    geo_mia = true;
  }
  if (dbc_service_mia_GEO_DEBUG_MSG(&geo_debug_msg, mia_increment_value)) {
    geo_mia = true;
  }
  if (dbc_service_mia_GEO_CURRENT_LOCATION_DEBUG(&geo_current_location_debug, mia_increment_value)) {
    geo_mia = true;
  }
  if (dbc_service_mia_MOTOR_DEBUG_MSG(&motor_debug_msg, mia_increment_value)) {
    motor_mia = true;
  }
}

static bool no_destination(dbc_GPS_DESTINATION_LOCATION_s *dest) {
  return (dest->GPS_DESTINATION_LOCATION_latitude == 0.0 && dest->GPS_DESTINATION_LOCATION_latitude == 0.0);
}

static bool is_idle(void) {
  return geo_debug_msg.GEO_DEBUG_MSG_lock_status || no_destination(&gps_destination_location) ||
         (current_geo_status.GEO_STATUS_distance_to_destination <= distance_stop_threshold);
}

static uint8_t lowest_sensor_reading(dbc_SENSOR_TO_DRIVER_SONARS_s *sense) {
  return MIN(sense->SENSOR_TO_DRIVER_SONARS_front_left,
             MIN(sense->SENSOR_TO_DRIVER_SONARS_front_middle, sense->SENSOR_TO_DRIVER_SONARS_front_right));
}

static uint8_t get_max_speed(uint8_t sensor_value) {
  if (sensor_value == no_obstacle) {
    return 31;
    // return max_motor_speed;
  } else if (sensor_value > ambiguous_obstacle) {
    return 23;
    // return ambiguous_obstacle_speed;
  } else if (sensor_value > close_obstacle) {
    return (sensor_value - close_obstacle) / (ambiguous_obstacle - close_obstacle) * (23 - slow_motor_speed) +
           slow_motor_speed;
    // return (sensor_value - close_obstacle) / (ambiguous_obstacle - close_obstacle) *
    //            (ambiguous_obstacle_speed - slow_motor_speed) + slow_motor_speed;
  } else if (sensor_value > emergency_stop_threshold) {
    return slow_motor_speed;
  } else {
    return 0; // update to 5
  }
}

static bool get_angular_difference(dbc_GEO_STATUS_s *geo, int16_t *angle) {
  if (no_destination(&gps_destination_location) || geo_debug_msg.GEO_DEBUG_MSG_lock_status == 1) {
    return false;
  }

  uint16_t compass_heading = geo->GEO_STATUS_compass_heading;
  uint16_t destination_heading = geo->GEO_STATUS_destination_heading;

  // TODO: this may need to slide towards destination_heading instead of attempting to jump straight to destination
  // heading on each compass reading
  *angle = destination_heading - compass_heading;
  if (*angle >= 180) {
    *angle -= 360;
  } else if (*angle < -180) {
    *angle += 360;
  }
  return true;
}

static obstacle_closest_e closest_obstacle(dbc_SENSOR_TO_DRIVER_SONARS_s *sense) {
  uint8_t lowest_value = lowest_sensor_reading(sense);
  if (lowest_value == sense->SENSOR_TO_DRIVER_SONARS_front_middle) {
    return CENTER;
  } else if (lowest_value == sense->SENSOR_TO_DRIVER_SONARS_front_right) {
    return RIGHT;
  } else {
    return LEFT;
  }
}

static int16_t obstacle_avoid_direction_when_reversing(dbc_SENSOR_TO_DRIVER_SONARS_s *sense) {
  // TODO: ANALOG REVERSE
  uint8_t lowest_value = lowest_sensor_reading(sense);
  if (lowest_value == sense->SENSOR_TO_DRIVER_SONARS_front_middle) {
    return 0;
  } else if (lowest_value == sense->SENSOR_TO_DRIVER_SONARS_front_right) {
    return 90;
  } else {
    return -90;
  }
}

static uint8_t obstacle_deflection_value(uint8_t sense_val) {
  float net_val = start_deflection - sense_val;
  if (net_val > 90) {
    return 50;
  } else {
    return (int)((50.0 / (90)) * (net_val));
  }
}

static bool estop_logic(dbc_DRIVER_TO_MOTOR_CMD_s *motor, uint8_t lowest_sensor_val, int16_t angle) {
  static int32_t counter = 0;
  bool still_in_estop = true;
  if (counter <= ESTOP_CHECK_DURATION) {
    if (lowest_sensor_val > emergency_stop_threshold) {
      still_in_estop = false;
    }
  } else {
    if (lowest_sensor_val > reverse_until_obstacle_threshold) {
      counter = -1; // Go back to initial ESTOP state
    } else if (sensor_data.SENSOR_TO_DRIVER_SONARS_back > emergency_stop_threshold) {
      motor_command.DRIVER_TO_MOTOR_speed = reverse_slow;
      if (angle <= -90 || angle >= 90) {
        motor_command.DRIVER_TO_MOTOR_steer = -obstacle_avoid_direction_when_reversing(&sensor_data);
      } else {
        motor_command.DRIVER_TO_MOTOR_steer = obstacle_avoid_direction_when_reversing(&sensor_data);
      }
    } else { // sensor_data.SENSOR_TO_DRIVER_SONARS_back <= emergency_stop_threshold
      printf("STUCK!!!!!!!!\n");
      counter = -1; // Go back to initial ESTOP state
    }
  }

  if (still_in_estop) {
    counter++;
  } else {
    counter = 0;
  }
  return still_in_estop;
}

static void actual_drive_logic(int16_t angle) {
  static bool estop = false;
  motor_command.DRIVER_TO_MOTOR_steer = 0;
  motor_command.DRIVER_TO_MOTOR_speed = 0; // becomes 5 later
  uint8_t low_sensor_value = lowest_sensor_reading(&sensor_data);
  // printf("%i %i\n", state, angle);

  // Universal idle condition; overrides ESTOP
  if (is_idle()) {
    if (angle < -50 || angle > 50 || low_sensor_value > start_deflection) {
      // Then we're literally going to ignore obstacle avoidance until we absolutely have to
      motor_command.DRIVER_TO_MOTOR_steer = angle;
    } else {
      uint8_t deflection_scale = obstacle_deflection_value(low_sensor_value);
      obstacle_closest_e obst = closest_obstacle(&sensor_data);
      if (obst == LEFT || (obst == CENTER && angle >= 0)) {
        motor_command.DRIVER_TO_MOTOR_steer = angle + deflection_scale;
      } else {
        motor_command.DRIVER_TO_MOTOR_steer = angle - deflection_scale;
      }
    }
  } else if (estop) {
    estop = estop_logic(&motor_command, low_sensor_value, angle);
  } else if (low_sensor_value <= emergency_stop_threshold) {
    // Use null default above
    estop = true;
  } else {
    motor_command.DRIVER_TO_MOTOR_speed = get_max_speed(low_sensor_value);

    if (angle < -50 || angle > 50 || low_sensor_value > start_deflection) {
      // Then we're literally going to ignore obstacle avoidance until we absolutely have to
      motor_command.DRIVER_TO_MOTOR_steer = angle;
    } else {
      uint8_t deflection_scale = obstacle_deflection_value(low_sensor_value);
      obstacle_closest_e obst = closest_obstacle(&sensor_data);
      if (obst == LEFT || (obst == CENTER && angle >= 0)) {
        motor_command.DRIVER_TO_MOTOR_steer = angle + deflection_scale;
      } else {
        motor_command.DRIVER_TO_MOTOR_steer = angle - deflection_scale;
      }
    }
  }
}

void driver_logic__update_leds(uint32_t callback_count) {
  update_gps_lock_led();
  update_app_connected_led();
  update_obstacle_leds(&sensor_data, callback_count);
  update_taillights(callback_count);
}

void driver_logic__process_geo_status(uint32_t callback_count) {
  int16_t angle = 0;
  bool is_angle_valid = get_angular_difference(&current_geo_status, &angle);
  led_ring_handler_compass(current_geo_status.GEO_STATUS_compass_heading, is_angle_valid ? angle + 180 : 0xFFFF,
                           current_geo_status.GEO_STATUS_distance_to_destination, 0xE1, 0x00, 0x00, 0xFF);
  actual_drive_logic(angle);
  motor_command.DRIVER_TO_MOTOR_current_rpm = wheel_encoder_data.SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_rpm;
}

dbc_DRIVER_TO_MOTOR_CMD_s driver_logic__get_motor_command(void) { return motor_command; }

void obstacle_detection_leds_init(void) {
  gpio__construct_as_output(front_left_obstacle_led.port_number, front_left_obstacle_led.pin_number);
  gpio__construct_as_output(front_middle_obstacle_led.port_number, front_middle_obstacle_led.pin_number);
  gpio__construct_as_output(front_right_obstacle_led.port_number, front_right_obstacle_led.pin_number);
  gpio__construct_as_output(back_obstacle_led.port_number, back_obstacle_led.pin_number);
}

void external_leds_init(void) {
  gpio__construct_as_output(gps_lock_led.port_number, gps_lock_led.pin_number);
  gpio__construct_as_output(app_connected_led.port_number, app_connected_led.pin_number);

  gpio__construct_as_output(headlight_left.port_number, headlight_left.pin_number);
  gpio__construct_as_output(headlight_right.port_number, headlight_right.pin_number);
  gpio__construct_as_output(taillight_right.port_number, taillight_right.pin_number);
  gpio__construct_as_output(taillight_left.port_number, taillight_left.pin_number);
  gpio__set(headlight_left);
  gpio__set(headlight_right);
  gpio__set(taillight_left);
  gpio__set(taillight_right);
}

void lcd_debug_menu_init(void) {
  gpio__construct_as_input(sw3.port_number, sw3.pin_number);
  gpio__construct_as_input(sw2.port_number, sw2.pin_number);
  gpio__construct_as_input(sw1.port_number, sw1.pin_number);
  gpio__construct_as_input(sw0.port_number, sw0.pin_number);
}

void set_sensor_data(dbc_SENSOR_TO_DRIVER_SONARS_s data) { sensor_data = data; }

void set_geo_status(dbc_GEO_STATUS_s geo_status) { current_geo_status = geo_status; }

void update_gps_lock_led(void) {
  if (geo_debug_msg.GEO_DEBUG_MSG_lock_status == 0) {
    gpio__set(gps_lock_led);
  } else {
    gpio__reset(gps_lock_led);
  }
}

void update_app_connected_led(void) {
  if (app_connection_msg.SENSOR_TO_DRIVER_APP_CONNECTION_MSG_app_connected == 0) {
    gpio__reset(app_connected_led);
  } else {
    gpio__set(app_connected_led);
  }
}

void update_taillights(uint32_t callback_count) {
  if (reverse_fast <= motor_command.DRIVER_TO_MOTOR_speed && motor_command.DRIVER_TO_MOTOR_speed <= reverse_slow) {
    if ((callback_count % 2) == 0) {
      gpio__toggle(taillight_left);
      gpio__toggle(taillight_right);
    }
  } else if (motor_command.DRIVER_TO_MOTOR_speed == idle_DO_NOT_USE ||
             motor_command.DRIVER_TO_MOTOR_speed == motor_stopped) {
    gpio__set(taillight_left);
    gpio__set(taillight_right);
  } else {
    gpio__reset(taillight_left);
    gpio__reset(taillight_right);
  }
}

static void display_debug_menu_on_lcd(uint32_t callback_count) {
  switch (debug_menu_state) {
  case START:
    display_start(sensor_mia, geo_mia, motor_mia);
    break;
  case LICENSE_PLATE:
    display_license_plate();
    break;
  case MAIN_MENU_1:
    display_main_menu_1();
    break;
  case MAIN_MENU_2:
    display_main_menu_2();
    break;
  case MAIN_MENU_3:
    display_main_menu_3();
    break;
  case MAIN_MENU_4:
    display_main_menu_4();
    break;
  case MAIN_MENU_5:
    display_main_menu_5();
    break;
  case SENSOR_DATA:
    display_sensor_data(callback_count, sensor_data.SENSOR_TO_DRIVER_SONARS_front_left,
                        sensor_data.SENSOR_TO_DRIVER_SONARS_front_middle,
                        sensor_data.SENSOR_TO_DRIVER_SONARS_front_right, sensor_data.SENSOR_TO_DRIVER_SONARS_back);
    break;
  case GEO_STATUS:
    display_geo_status(callback_count, current_geo_status.GEO_STATUS_compass_heading,
                       current_geo_status.GEO_STATUS_destination_heading,
                       current_geo_status.GEO_STATUS_distance_to_destination);
    break;
  case GPS_INFO:
    display_gps_info(callback_count, geo_debug_msg.GEO_DEBUG_MSG_lock_status,
                     geo_debug_msg.GEO_DEBUG_MSG_num_satellites);
    break;
  case LOCATION_INFO:
    display_location_info(callback_count, geo_current_location_debug.GEO_CURRENT_LOCATION_DEBUG_latitude,
                          geo_current_location_debug.GEO_CURRENT_LOCATION_DEBUG_longitude,
                          gps_destination_location.GPS_DESTINATION_LOCATION_latitude,
                          gps_destination_location.GPS_DESTINATION_LOCATION_longitude);
    break;
  case MOTOR_COMMANDS:
    display_motor_commands(callback_count, motor_command.DRIVER_TO_MOTOR_steer, motor_command.DRIVER_TO_MOTOR_speed,
                           motor_debug_msg.MOTOR_DEBUG_MSG_echo_steer, motor_debug_msg.MOTOR_DEBUG_MSG_echo_speed,
                           wheel_encoder_data.SENSOR_TO_DRIVER_WHEEL_ENCODER_MSG_rpm);
    break;
  default:
    display_start(sensor_mia, geo_mia, motor_mia);
    break;
  }
}

static void handle_debug_menu_navigation(void) {
  bool sw3_value = gpio__get(sw3);
  bool sw2_value = gpio__get(sw2);
  bool sw1_value = gpio__get(sw1);
  bool sw0_value = gpio__get(sw0);

  if (!key_pressed && validate_key_press(sw3_value, sw2_value, sw1_value, sw0_value)) {
    key_pressed = true;
    update_pressed_key(sw3_value, sw2_value, sw1_value, sw0_value);
    update_debug_menu_state();
    lcd_display__set_display_menu_debug_changed(true);
  } else if (key_pressed) {
    update_key_pressed_on_key_unpressed(sw3_value, sw2_value, sw1_value, sw0_value);
    lcd_display__set_display_menu_debug_changed(false);
  } else {
    lcd_display__set_display_menu_debug_changed(false);
  }
}

void driver_logic__manage_debug_menu_10hz(uint32_t callback_count) {
  display_debug_menu_on_lcd(callback_count);
  handle_debug_menu_navigation();
}

bool validate_key_press(bool sw3_value, bool sw2_value, bool sw1_value, bool sw0_value) {
  bool valid_key_press = false;
  if ((sw3_value && !sw2_value && !sw1_value && !sw0_value) || (!sw3_value && sw2_value && !sw1_value && !sw0_value) ||
      (!sw3_value && !sw2_value && sw1_value && !sw0_value) || (!sw3_value && !sw2_value && !sw1_value && sw0_value)) {
    valid_key_press = true;
  }
  return valid_key_press;
}

void update_pressed_key(bool sw3_value, bool sw2_value, bool sw1_value, bool sw0_value) {
  if (sw3_value) {
    pressed_key = SW3;
  } else if (sw2_value) {
    pressed_key = SW2;
  } else if (sw1_value) {
    pressed_key = SW1;
  } else if (sw0_value) {
    pressed_key = SW0;
  }
}

void update_key_pressed_on_key_unpressed(bool sw3_value, bool sw2_value, bool sw1_value, bool sw0_value) {
  switch (pressed_key) {
  case SW3:
    if (!sw3_value) {
      key_pressed = false;
    }
    break;
  case SW2:
    if (!sw2_value) {
      key_pressed = false;
    }
    break;
  case SW1:
    if (!sw1_value) {
      key_pressed = false;
    }
    break;
  case SW0:
    if (!sw0_value) {
      key_pressed = false;
    }
    break;
  }
}

void update_debug_menu_state(void) {
  switch (debug_menu_state) {
  case START:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = LICENSE_PLATE;
      break;
    case SW3:
      debug_menu_state = MAIN_MENU_1;
      break;
    default:
      break;
    }
    break;
  case LICENSE_PLATE:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = START;
      break;
    default:
      break;
    }
    break;
  case MAIN_MENU_1:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = START;
      break;
    case SW2:
      debug_menu_state = MAIN_MENU_2;
      break;
    case SW3:
      debug_menu_state = SENSOR_DATA;
      break;
    default:
      break;
    }
    break;
  case MAIN_MENU_2:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = START;
      break;
    case SW1:
      debug_menu_state = MAIN_MENU_1;
      break;
    case SW2:
      debug_menu_state = MAIN_MENU_3;
      break;
    case SW3:
      debug_menu_state = GEO_STATUS;
      break;
    default:
      break;
    }
    break;
  case MAIN_MENU_3:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = START;
      break;
    case SW1:
      debug_menu_state = MAIN_MENU_2;
      break;
    case SW2:
      debug_menu_state = MAIN_MENU_4;
      break;
    case SW3:
      debug_menu_state = GPS_INFO;
      break;
    }
    break;
  case MAIN_MENU_4:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = START;
      break;
    case SW1:
      debug_menu_state = MAIN_MENU_3;
      break;
    case SW2:
      debug_menu_state = MAIN_MENU_5;
      break;
    case SW3:
      debug_menu_state = LOCATION_INFO;
      break;
    default:
      break;
    }
    break;
  case MAIN_MENU_5:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = START;
      break;
    case SW1:
      debug_menu_state = MAIN_MENU_4;
      break;
    case SW3:
      debug_menu_state = MOTOR_COMMANDS;
      break;
    default:
      break;
    }
    break;
  case SENSOR_DATA:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = MAIN_MENU_1;
      break;
    default:
      break;
    }
    break;
  case GEO_STATUS:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = MAIN_MENU_2;
      break;
    default:
      break;
    }
    break;
  case GPS_INFO:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = MAIN_MENU_3;
      break;
    default:
      break;
    }
    break;
  case LOCATION_INFO:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = MAIN_MENU_4;
      break;
    default:
      break;
    }
    break;
  case MOTOR_COMMANDS:
    switch (pressed_key) {
    case SW0:
      debug_menu_state = MAIN_MENU_5;
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }
}
