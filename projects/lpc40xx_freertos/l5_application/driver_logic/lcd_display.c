#include "lcd_display.h"

#include "i2c.h"
#include "i2c_lcd.h"

#include <stdio.h>

static bool debug_menu_state_changed = true;

void lcd_display__set_display_menu_debug_changed(bool value) { debug_menu_state_changed = value; }

void display_start(bool sensor_mia, bool geo_mia, bool motor_mia) {
  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("SW3- DEBUG INFO     ");
  }
  i2c_lcd__starting_position(1, 0);
  if (sensor_mia) {
    i2c_lcd__printf("SENSOR node is MIA! ");
  } else {
    i2c_lcd__printf("                    ");
  }
  i2c_lcd__starting_position(2, 0);
  if (geo_mia) {
    i2c_lcd__printf("GEO node is MIA!    ");
  } else {
    i2c_lcd__printf("                    ");
  }
  i2c_lcd__starting_position(3, 0);
  if (motor_mia) {
    i2c_lcd__printf("MOTOR node is MIA!  ");
  } else {
    i2c_lcd__printf("                    ");
  }
}

void display_license_plate(void) {
  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("NOV   CALIFORNIA  22");
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("                    ");
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("     CURVY CARLA    ");
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("                    ");
  }
}

void display_main_menu_1(void) {
  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("SW0- BACK           ");
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("  -->SENSOR DATA    ");
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("     GEO STATUS     ");
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("SW3- SELECT         ");
  }
}

void display_main_menu_2(void) {
  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("SW0- BACK           ");
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("  -->GEO STATUS     ");
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("     GPS INFO       ");
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("SW3- SELECT         ");
  }
}

void display_main_menu_3(void) {
  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("SW0- BACK           ");
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("  -->GPS INFO       ");
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("     LOCATION INFO  ");
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("SW3- SELECT         ");
  }
}

void display_main_menu_4(void) {
  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("SW0- BACK           ");
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("  -->LOCATION INFO  ");
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("     MOTOR COMMANDS ");
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("SW3- SELECT         ");
  }
}

void display_main_menu_5(void) {
  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("SW0- BACK           ");
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("  -->MOTOR COMMANDS ");
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("                    ");
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("SW3- SELECT         ");
  }
}

void display_sensor_data(uint32_t callback_count, uint8_t front_left, uint8_t front_middle, uint8_t front_right,
                         uint8_t back) {
  char front_left_str[10], front_middle_str[10], front_right_str[10], back_str[10];

  switch (callback_count % 4) {
  case 0:
    sprintf(front_left_str, "%d   ", front_left);
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("Front Left=         ");
    i2c_lcd__starting_position(0, 14);
    i2c_lcd__printf(front_left_str);
    i2c_lcd__starting_position(0, 18);
    i2c_lcd__printf("cm");
    break;
  case 1:
    sprintf(front_middle_str, "%d   ", front_middle);
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("Front Middle=       ");
    i2c_lcd__starting_position(1, 14);
    i2c_lcd__printf(front_middle_str);
    i2c_lcd__starting_position(1, 18);
    i2c_lcd__printf("cm");
    break;
  case 2:
    sprintf(front_right_str, "%d   ", front_right);
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("Front Right=        ");
    i2c_lcd__starting_position(2, 14);
    i2c_lcd__printf(front_right_str);
    i2c_lcd__starting_position(2, 18);
    i2c_lcd__printf("cm");
    break;
  case 3:
    sprintf(back_str, "%d   ", back);
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("Back=               ");
    i2c_lcd__starting_position(3, 14);
    i2c_lcd__printf(back_str);
    i2c_lcd__starting_position(3, 18);
    i2c_lcd__printf("cm");
    break;
  }
}

void display_geo_status(uint32_t callback_count, uint16_t compass_heading, uint16_t destination_heading,
                        float distance_to_destination) {
  char compass_heading_str[10], destination_heading_str[10], distance_to_destination_str[10];

  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("                    ");
  }

  switch (callback_count % 4) {
  case 0:
    sprintf(compass_heading_str, "%d", compass_heading);
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("Compass Heading=    ");
    i2c_lcd__starting_position(0, 17);
    i2c_lcd__printf(compass_heading_str);
    break;
  case 1:
    sprintf(destination_heading_str, "%d", destination_heading);
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("Dest Heading=       ");
    i2c_lcd__starting_position(1, 17);
    i2c_lcd__printf(destination_heading_str);
    break;
  case 2:
    sprintf(distance_to_destination_str, "%.1f", distance_to_destination);
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("Destination=        ");
    i2c_lcd__starting_position(2, 13);
    i2c_lcd__printf(distance_to_destination_str);
    i2c_lcd__starting_position(2, 19);
    i2c_lcd__printf("m");
    break;
  default:
    break;
  }
}

void display_gps_info(uint32_t callback_count, uint8_t lock_status, uint8_t num_satellites) {
  char num_satellites_str[10];

  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("                    ");
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("                    ");
  }

  switch (callback_count % 4) {
  case 0:
    i2c_lcd__starting_position(0, 0);
    if (lock_status == 0) {
      i2c_lcd__printf("GPS LOCK= LOCKED    ");
    } else {
      i2c_lcd__printf("GPS LOCK= NOT LOCKED");
    }
    break;
  case 1:
    sprintf(num_satellites_str, "%d", num_satellites);
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("Num Satellites=     ");
    i2c_lcd__starting_position(1, 16);
    i2c_lcd__printf(num_satellites_str);
    break;
  default:
    break;
  }
}

void display_location_info(uint32_t callback_count, float current_latitude, float current_longitude,
                           float destination_latitude, float destination_longitude) {
  char current_latitude_str[10], current_longitude_str[10], destination_latitude_str[10], destination_longitude_str[10];

  switch (callback_count % 4) {
  case 0:
    sprintf(current_latitude_str, "%f", current_latitude);
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("Cur Lat=            ");
    i2c_lcd__starting_position(0, 8);
    i2c_lcd__printf(current_latitude_str);
    break;
  case 1:
    sprintf(current_longitude_str, "%f", current_longitude);
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("Cur Long=           ");
    i2c_lcd__starting_position(1, 9);
    i2c_lcd__printf(current_longitude_str);
    break;
  case 2:
    sprintf(destination_latitude_str, "%f", destination_latitude);
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("Dest Lat=           ");
    i2c_lcd__starting_position(2, 9);
    i2c_lcd__printf(destination_latitude_str);
    break;
  case 3:
    sprintf(destination_longitude_str, "%f", destination_longitude);
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("Dest Long=          ");
    i2c_lcd__starting_position(3, 10);
    i2c_lcd__printf(destination_longitude_str);
    break;
  }
}

void display_motor_commands(uint32_t callback_count, int16_t motor_command_steer, uint8_t motor_command_speed,
                            int16_t motor_debug_steer, uint8_t motor_debug_speed, uint16_t rpm) {
  char motor_command_steer_str[10], motor_command_speed_str[10], motor_debug_steer_str[10], motor_debug_speed_str[10],
      rpm_str[10];

  if (debug_menu_state_changed) {
    i2c_lcd__starting_position(3, 0);
    i2c_lcd__printf("                    ");
  }

  switch (callback_count % 4) {
  case 0:
    sprintf(motor_command_steer_str, "%d", motor_command_steer);
    sprintf(motor_command_speed_str, "%d", motor_command_speed);
    i2c_lcd__starting_position(0, 0);
    i2c_lcd__printf("D Steer=    Speed=  ");
    i2c_lcd__starting_position(0, 8);
    i2c_lcd__printf(motor_command_steer_str);
    i2c_lcd__starting_position(0, 18);
    i2c_lcd__printf(motor_command_speed_str);
    break;
  case 1:
    sprintf(motor_debug_steer_str, "%d", motor_debug_steer);
    sprintf(motor_debug_speed_str, "%d", motor_debug_speed);
    i2c_lcd__starting_position(1, 0);
    i2c_lcd__printf("M Steer=    Speed=  ");
    i2c_lcd__starting_position(1, 8);
    i2c_lcd__printf(motor_debug_steer_str);
    i2c_lcd__starting_position(1, 18);
    i2c_lcd__printf(motor_debug_speed_str);
    break;
  case 2:
    sprintf(rpm_str, "%d", rpm);
    i2c_lcd__starting_position(2, 0);
    i2c_lcd__printf("RPM=                ");
    i2c_lcd__starting_position(2, 8);
    i2c_lcd__printf(rpm_str);
    break;
  default:
    break;
  }
}