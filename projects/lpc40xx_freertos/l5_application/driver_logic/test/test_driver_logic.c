#include "unity.h"

#include "project.h"

#include "can_mia_configurations.h"

#include "Mockcan_bus.h"
#include "Mockgpio.h"
#include "Mocki2c_lcd.h"
#include "Mocklcd_display.h"
#include "Mockled_ring.h"
#include "driver_logic.h"

void reset_driver_logic(void) {}

void setUp(void) {}

void tearDown(void) {}

// void test__driver_logic__process_sensor_data(void) {
//   // Simulate DRIVER node decoding SENSOR_TO_DRIVER_SONAR message
//   dbc_SENSOR_TO_DRIVER_SONARS_s sensor_data = {.SENSOR_TO_DRIVER_SONARS_front_left = object_detection_threshold + 1,
//                                                .SENSOR_TO_DRIVER_SONARS_front_middle = object_detection_threshold +
//                                                1, .SENSOR_TO_DRIVER_SONARS_front_right = object_detection_threshold +
//                                                1, .SENSOR_TO_DRIVER_SONARS_back = object_detection_threshold + 1};
//   set_sensor_data(sensor_data);
//   // Expect NO_OBSTACLES LEDs
//   gpio__reset_ExpectAnyArgs();
//   gpio__reset_ExpectAnyArgs();
//   gpio__reset_ExpectAnyArgs();
//   gpio__reset_ExpectAnyArgs();
//   driver_logic__process_sensor_data();
//   // Verify DRIVER's internal driver_to_motor_cmd is not updated
//   driver_to_motor_cmd = driver_logic__get_motor_command();
//   TEST_ASSERT_EQUAL(0, driver_to_motor_cmd.DRIVER_TO_MOTOR_steer);
//   TEST_ASSERT_EQUAL(0, driver_to_motor_cmd.DRIVER_TO_MOTOR_speed);
// }

// void test__driver_logic__process_sensor_data_steer_left(void) {
//   // Simulate DRIVER node decoding SENSOR_TO_DRIVER_SONAR message
//   dbc_SENSOR_TO_DRIVER_SONARS_s sensor_data = {.SENSOR_TO_DRIVER_SONARS_front_left = object_detection_threshold + 1,
//                                                .SENSOR_TO_DRIVER_SONARS_front_middle = object_detection_threshold +
//                                                1, .SENSOR_TO_DRIVER_SONARS_front_right = object_detection_threshold,
//                                                .SENSOR_TO_DRIVER_SONARS_back = object_detection_threshold + 1};
//   set_sensor_data(sensor_data);
//   // Expect FRONT_RIGHT obstacle LEDs
//   gpio__reset_ExpectAnyArgs();
//   gpio__reset_ExpectAnyArgs();
//   gpio__set_ExpectAnyArgs();
//   gpio__reset_ExpectAnyArgs();
//   driver_logic__process_sensor_data();
//   // Verify DRIVER's internal driver_to_motor_cmd is updated to steer left and speed set to slow_motor_speed when
//   // starting from 0 motor speed
//   driver_to_motor_cmd = driver_logic__get_motor_command();
//   TEST_ASSERT_EQUAL(0 - steer_adjustment_value, driver_to_motor_cmd.DRIVER_TO_MOTOR_steer);
//   TEST_ASSERT_EQUAL(slow_motor_speed, driver_to_motor_cmd.DRIVER_TO_MOTOR_speed);
// }

// void test__driver_logic__process_sensor_data_steer_right(void) {
//   // Start at max speed
//   set_motor_speed(max_motor_speed);
//   // Simulate DRIVER node decoding SENSOR_TO_DRIVER_SONAR message
//   dbc_SENSOR_TO_DRIVER_SONARS_s sensor_data = {.SENSOR_TO_DRIVER_SONARS_front_left = object_detection_threshold,
//                                                .SENSOR_TO_DRIVER_SONARS_front_middle = object_detection_threshold +
//                                                1, .SENSOR_TO_DRIVER_SONARS_front_right = object_detection_threshold +
//                                                1, .SENSOR_TO_DRIVER_SONARS_back = object_detection_threshold + 1};
//   set_sensor_data(sensor_data);
//   // Expect FRONT_LEFT obstacle LEDs
//   gpio__reset_ExpectAnyArgs();
//   gpio__reset_ExpectAnyArgs();
//   gpio__set_ExpectAnyArgs();
//   gpio__reset_ExpectAnyArgs();
//   driver_logic__process_sensor_data();
//   // Verify DRIVER's internal driver_to_motor_cmd is updated to steer right and slow down
//   driver_to_motor_cmd = driver_logic__get_motor_command();
//   TEST_ASSERT_EQUAL(0 + steer_adjustment_value, driver_to_motor_cmd.DRIVER_TO_MOTOR_steer);
//   TEST_ASSERT_EQUAL((float)max_motor_speed * slowdown_rate, driver_to_motor_cmd.DRIVER_TO_MOTOR_speed);
// }

// void test__driver_logic__process_geo_status_distance_slowdown_threshold(void) {
//   // Start at max speed
//   set_motor_speed(max_motor_speed);
//   // Simulate DRIVER node decoding GEO_STATUS message with distance_slowdown_threshold met
//   dbc_GEO_STATUS_s geo_status = {.GEO_STATUS_compass_heading = 0,
//                                  .GEO_STATUS_destination_heading = 0,
//                                  .GEO_STATUS_distance_to_destination = distance_slowdown_threshold};
//   set_geo_status(geo_status);
//   // Expect GPS Lock LED
//   gpio__set_ExpectAnyArgs();
//   led_ring_starting_frame_Expect();
//   led_ring_handler_compass_ExpectAnyArgs();
//   led_ring_ending_frame_Expect();
//   driver_logic__process_geo_status(0);
//   // Verify DRIVER's internal driver_to_motor_cmd is updated to slow down
//   driver_to_motor_cmd = driver_logic__get_motor_command();
//   TEST_ASSERT_EQUAL((float)max_motor_speed * slowdown_rate, driver_to_motor_cmd.DRIVER_TO_MOTOR_speed);
// }

// void test__driver_logic__process_geo_status_distance_stop_threshold(void) {
//   // Start at max speed
//   set_motor_speed(max_motor_speed);
//   // Simulate DRIVER node decoding GEO_STATUS message with distance_stop_threshold met
//   dbc_GEO_STATUS_s geo_status = {.GEO_STATUS_compass_heading = 0,
//                                  .GEO_STATUS_destination_heading = 0,
//                                  .GEO_STATUS_distance_to_destination = distance_stop_threshold};
//   set_geo_status(geo_status);
//   // Expect GPS Lock LED
//   gpio__set_ExpectAnyArgs();
//   led_ring_starting_frame_Expect();
//   led_ring_handler_compass_ExpectAnyArgs();
//   led_ring_ending_frame_Expect();
//   driver_logic__process_geo_status(0);
//   // When DRIVER's internal driver_to_motor_cmd is updated to stop
//   driver_to_motor_cmd = driver_logic__get_motor_command();
//   TEST_ASSERT_EQUAL(0, driver_to_motor_cmd.DRIVER_TO_MOTOR_speed);
// }