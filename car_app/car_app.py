# import math
import os
from PIL import Image, ImageTk
import queue
import threading
import time
import tkinter as tk

from enum import IntEnum, auto


class Maps(IntEnum):
    BIG = 0
    SMALL = auto()
    CAMPUS = auto()
    COUNT = auto()


map_dict = {
    Maps.BIG: {
        'coords': {
            'top_left': (42.257923, -126.708527),
            'bottom_right': (32.105504, -112.818561),
            'dimensions': (1000, 920)
        },
        'img_path': os.path.join(os.path.dirname(__file__), 'bigmap.png'),
        'img_data': None
    },
    Maps.SMALL: {
        'coords': {
            'top_left': (37.339309, -121.887050),
            'bottom_right': (37.330863, -121.875505),
            'dimensions': (1000, 920)
        },
        'img_path': os.path.join(os.path.dirname(__file__),'smallmap.png'),
        'img_data': None
    },
    Maps.CAMPUS: {
        'coords': {
            'top_left': (37.3401061518164, -121.88187976097434),
            'bottom_right': (37.33833829859779,  -121.87945772627349),
            'dimensions': (1000, 920)
        },
        'img_path': os.path.join(os.path.dirname(__file__),'garagemap.png'),
        'img_data': None
    }
}


def get_xy_pos(map_dict, coord_pair):
    return (int((coord_pair[1]-map_dict['top_left'][1])/(map_dict['bottom_right'][1]-map_dict['top_left'][1])*map_dict['dimensions'][0]),
            int((coord_pair[0]-map_dict['top_left'][0])/(map_dict['bottom_right'][0]-map_dict['top_left'][0])*map_dict['dimensions'][1]))


def get_coords(map_dict, xy_pair):
    return (map_dict['top_left'][0]+xy_pair[1]*(map_dict['bottom_right'][0]-map_dict['top_left'][0])/map_dict['dimensions'][1],
            map_dict['top_left'][1]+xy_pair[0]*(map_dict['bottom_right'][1]-map_dict['top_left'][1])/map_dict['dimensions'][0])
    # return (int((coord_pair[1]-map_dict['top_left'][1])/(map_dict['bottom_right'][1]-map_dict['top_left'][1])*map_dict['dimensions'][0]),
    #         int((coord_pair[0]-map_dict['top_left'][0])/(map_dict['bottom_right'][0]-map_dict['top_left'][0])*map_dict['dimensions'][1]))


class Flag:

    def __init__(self):
        self.position = None
        self.active = False
        self.markers = []
        self.primary = True


class Car:

    def __init__(self):
        self.position = None
        self.direction = None
        self.current_dest = None
        self.dest_direction = None
        self.dest_distance = None
        self.active = False
        self.missing = False
        self.gps_lock = False
        self.markers = []
        self.sensor_l = None
        self.sensor_m = None
        self.sensor_r = None
        self.sensor_b = None
        self.rpm = None

    def update(self, in_str):
        msg = in_str.decode('utf-8').split(',')
        if (len(msg) != 12):
            self.missing = True
            print(f"???BADEGG??? {msg}")
        else:
            self.position = (float(msg[0]), float(msg[1]))
            self.direction = int(msg[2])
            self.active = True
            self.missing = False
            self.gps_lock = self.position != (0.0, 0.0)
            self.dest_direction = int(msg[3])
            self.dest_distance = float(msg[4])
            self.rpm = int(msg[5])
            self.sensor_l = int(msg[6])
            self.sensor_m = int(msg[7])
            self.sensor_r = int(msg[8])
            self.sensor_b = int(msg[9])
            self.current_dest = (float(msg[10]), float(msg[11]))


class CarApp(tk.Tk):

    def __init__(self):
        self.loc = None
        self.window = tk.Tk.__init__(self)
        self.canvas = tk.Canvas(self.window, width=1000, height=920, highlightthickness=0, relief='ridge')
        self.canvas.pack_propagate(False)
        self.canvas.grid()
        self.title('Vroom Vroom')

        # jank code jank code
        map_dict[Maps.BIG]['img_data'] = ImageTk.PhotoImage(Image.open(map_dict[Maps.BIG]['img_path']))
        map_dict[Maps.SMALL]['img_data'] = ImageTk.PhotoImage(Image.open(map_dict[Maps.SMALL]['img_path']))
        map_dict[Maps.CAMPUS]['img_data'] = ImageTk.PhotoImage(Image.open(map_dict[Maps.CAMPUS]['img_path']))

        self.map_id = Maps.BIG
        self.image = self.canvas.create_image(0, 0, anchor='nw', image=map_dict[self.map_id]['img_data'])

        self.bind('<Return>', self.enter)
        self.bind('<Button-1>', self.update_flag)
        self.protocol("WM_DELETE_WINDOW", self.ender)
        self.bind('<space>', self.space_pressed)
        self.bind('<<CarUpdate>>', self.update_map)
        # self.bind('<Control-Shift-space>', self.space_pressed)
        # self.protocol("WM_DELETE_WINDOW", self.on_delete)

        self.flag = Flag()
        self.car = Car()
        self.loc_queue = queue.Queue()
        self.event = threading.Event()
        self.data_task = DataTask(self.loc_queue, self.event, self)
        self.data_task.start()

        self.mainloop()

    def update_map(self, *args):
        self.clear_flag_marker()
        self.draw_flag_marker()
        self.clear_car_marker()
        self.draw_car_marker()

    def enter(self, *args):
        self.map_id = (self.map_id + 1) % Maps.COUNT
        self.canvas.itemconfigure(self.image, image=map_dict[self.map_id]['img_data'])
        self.update_map()

    def space_pressed(self, *args):
        self.loc_queue.put((0, 0))
        self.flag.active = False
        self.clear_flag_marker()

    def update_flag(self, event):
        self.flag.position = get_coords(map_dict[self.map_id]['coords'], (event.x, event.y))
        self.flag.active = True
        self.loc_queue.put(self.flag.position)
        self.clear_flag_marker()
        self.draw_flag_marker()

    def ender(self):
        self.event.set()
        self.destroy()
        self.data_task.join()

    def clear_car_marker(self):
        for item in self.car.markers:
            self.canvas.delete(item)

    def clear_flag_marker(self):
        for item in self.flag.markers:
            self.canvas.delete(item)

    def draw_car_marker(self):
        if self.car.active:
            self.car.markers.append(self.canvas.create_text(40, 890, text=f"{self.car.rpm}", fill='black',
                                                            font=("Arial", 25, 'bold')))


            coords = map_dict[self.map_id]['coords']
            if self.car.position != (0, 0):
                car_x, car_y = get_xy_pos(coords, self.car.position)
                if (0 <= car_x < coords['dimensions'][0]) and (0 <= car_y < coords['dimensions'][1]):
                    pass
                    # car_x = 15
                    # car_y = 15
            else:
                car_x, car_y = coords['dimensions']
                car_x /= 2
                car_y /= 2

            for sensor, offset, color in ((self.car.sensor_l, 20, '#00FF00'),
                                          (self.car.sensor_r, -60, '#FFFF00'),
                                          (self.car.sensor_b, 160, '#0000FF'),
                                          (self.car.sensor_m, -20, 'red')):
                sensor_show = sensor/8 + 20
                self.car.markers.append(
                    self.canvas.create_arc(car_x - sensor_show, car_y - sensor_show, car_x + sensor_show, car_y + sensor_show, extent=40,
                                           start=(90-self.car.direction+360+offset)%360,
                                           outline=color if sensor <= 30 else '',
                                           fill='' if sensor <= 30 else color, width=2))

            corrected_dest_dir = (90 - self.car.dest_direction + 360 - 30) % 360
            self.car.markers.append(self.canvas.create_oval(car_x-15, car_y-15, car_x+15, car_y+15, fill='#000000', outline=''))
            self.car.markers.append(self.canvas.create_arc(car_x-15, car_y-15, car_x+15, car_y+15, extent=60,
                                                           start=(90-self.car.direction+360-30)%360, fill='gray', outline=''))
            self.car.markers.append(
                self.canvas.create_arc(car_x - 14, car_y - 14, car_x + 14, car_y + 14, extent=60, style='arc',
                                       start=corrected_dest_dir, outline='white', width=3))
            # if self.car.dest_distance:
            #     print(self.car.dest_distance)
            #     dest_offset_lat = self.car.position[0] + ((self.car.dest_distance / 500) * 0.009 * math.cos(corrected_dest_dir))
            #     dest_offset_lon = self.car.position[1] + ((self.car.dest_distance / 500) * 0.009 * math.sin(corrected_dest_dir))
            #     dest_offset_x, dest_offset_y = get_xy_pos(coords, (dest_offset_lat, dest_offset_lon))
            #     fmt_args = {'fill': 'green', 'width': 2}
            #     self.car.markers.append(self.canvas.create_line(dest_offset_x-10, dest_offset_y-10, dest_offset_x+10, dest_offset_y+10, **fmt_args))
            #     self.car.markers.append(self.canvas.create_line(dest_offset_x-10, dest_offset_y+10, dest_offset_x+10, dest_offset_y-10, **fmt_args))

            if self.car.current_dest and self.car.current_dest != self.flag.position:
                waypoint_x, waypoint_y = get_xy_pos(coords, self.car.current_dest)
                fmt_args = {'fill': 'green', 'width': 3}
                self.car.markers.append(self.canvas.create_line(waypoint_x-8, waypoint_y-8, waypoint_x+8, waypoint_y+8, **fmt_args))
                self.car.markers.append(self.canvas.create_line(waypoint_x-8, waypoint_y+8, waypoint_x+8, waypoint_y-8, **fmt_args))

            if self.car.missing:
                fmt_args = {'fill': 'red', 'width': 4}
                self.car.markers.append(self.canvas.create_line(car_x-15, car_y-15, car_x+15, car_y+15, **fmt_args))
                self.car.markers.append(self.canvas.create_line(car_x-15, car_y+15, car_x+15, car_y-15, **fmt_args))

            if not self.car.gps_lock:   # self.car.position and self.car.direction is not None:
                self.car.markers.append(self.canvas.create_text(car_x, car_y, text="?", anchor='center', fill='black',
                                                                font=("Arial", 25, 'bold')))
                self.car.markers.append(self.canvas.create_text(car_x, car_y, text="?", anchor='center', fill='black',
                                                                font=("Arial", 24, 'bold')))
                self.car.markers.append(self.canvas.create_text(car_x, car_y, text="?", anchor='center', fill='yellow',
                                                                font=("Arial", 24)))

    def draw_flag_marker(self):
        if self.flag.active:
            x, y = get_xy_pos(map_dict[self.map_id]['coords'], self.flag.position)
            self.flag.markers.append(self.canvas.create_line(x, y, x, y - 30, fill='black', width=3))
            self.flag.markers.append(self.canvas.create_polygon(x, y - 30, x + 15, y - 25, x, y - 20, fill='red'))
            # self.flag.markers.append(self.canvas.create_oval(x - 15, y - 15, x + 15,  y + 15, fill='black'))
            # self.flag.markers.append(self.canvas.create_arc(x - 15, y - 15, x + 15,  y + 15, extent=60,
            #                          start=(90 - 0 + 360 - 30) % 360, fill='white'))


class RecvTask(threading.Thread):

    def __init__(self, sock, queue):
        super().__init__()
        self.sock = sock
        self.queue = queue

    def run(self):
        try:
            while True:
                message, address = self.sock.recvfrom(1024)
                self.queue.put(message)
        except OSError:
            pass    # fucking cry baby


class DataTask(threading.Thread):

    def __init__(self, q, event, app):
        super().__init__()
        self.queue = q
        self.event = event
        self.app = app

    def run(self):
        import socket
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server.bind(('', 1766))
        q = queue.Queue()
        recv = RecvTask(server, q)
        recv.start()
        send_loc = (0,0)

        connected = False
        missed_count = 0
        while not self.event.is_set():
            try:
                send_loc = self.queue.get(timeout=0)
                print(f"New destination: {'' if connected else '(not connected) '}{send_loc}")
            except queue.Empty:
                pass

            try:
                sock.sendto('{},{}\n'.format(*send_loc).encode('ascii'), ('172.20.10.10', 1765))
                received_at_least_one_msg = False
                m = None
                try:
                    while True:
                        m = q.get(timeout=0 if received_at_least_one_msg else 0.2)
                        received_at_least_one_msg = True
                except:
                    if not received_at_least_one_msg:
                        raise
            except (OSError, queue.Empty):
                if connected:
                    if missed_count < 5:
                        missed_count += 1
                    else:
                        connected = False
                        self.app.car.missing = True
                        print("Board is not connected!")
                        self.app.event_generate("<<CarUpdate>>")
            else:
                missed_count = 0
                if not connected:
                    connected = True
                    print("Board is connected!")

                self.app.car.update(m)
                self.app.event_generate("<<CarUpdate>>") # , when="tail")
                time.sleep(0.1)

        sock.close()
        server.close()
        recv.join()


if __name__ == "__main__":
    CarApp()
